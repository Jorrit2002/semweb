import urllib.request
import json

class GeoNames:


    def __init__(self):
        username = "semwebIk"
        self.url = "http://api.geonames.org/search?username="+username+"&type=json&lang=nl&name="
        self.wikiURL = "http://api.geonames.org/wikipediaSearch?username="+username+"&lang=nl&type=json&q="

    def download(self, url):
        req = urllib.request.Request(url)
        with urllib.request.urlopen(url) as response:
            content = response.read().decode("utf-8")

        return json.loads(content)

    def findPlace(self, name):
        url = self.url+name

        return self.download(url)

    def findWiki(self, name):
        url = self.wikiURL+name
        return self.download(url)
