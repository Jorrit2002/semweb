
import sys
import os
import nltk
from nltk.tag.stanford import StanfordNERTagger

import json

from googlemaps import *
from geonames import *

def nerttagger(tokenized):


    st = StanfordNERTagger('stanford/classifiers/english.all.3class.distsim.crf.ser.gz', 'stanford/stanford-ner-3.4.jar')
    tag_list = st.tag(tokenized)

    return tag_list
    # print(tag_list)

def find_capital_words(tokenized):
    annotated = []

    for word in tokenized:
        if word[0].isupper():
            annotated.append((word, "upper"))
        else:
            annotated.append((word, "lower"))

    return annotated

def main():
    file_content = open('books/test.txt').read()
    tokenized = nltk.word_tokenize(file_content)

    capital_list = find_capital_words(tokenized)
    nert_list = nerttagger(tokenized)

    # print(cpaital_list)
    # print(nert_list)
    gn = GeoNames()

    locations = [value for value, tag in nert_list if tag == "LOCATION"]
    for location in locations:

        data = gn.findPlace(location)
        first_location = data["geonames"][0]["toponymName"]
        data = gn.findWiki(first_location)
        first_wiki = data["geonames"][0]["wikipediaUrl"]
        if first_wiki:
            print("{:<20} {:<20} {}".format(location, first_location, first_wiki))




    # for i in range(0, len(tokenized)):
    #     if nert_list[i][1] is not "PERSON" and capital_list[i][1] == "upper":
    #         print(tokenized[i])
    #
    # gm = GoogleMapsManager()
    # gm.findPlace("Leuvarden")
    #
    # find_capital_words()




if __name__ == '__main__':
    main()
