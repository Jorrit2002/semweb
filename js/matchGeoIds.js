// Every word in the text has been stored in the database with the following key
// tekstid\x00paragraphIndex\x00sentenceIndex\x00wordIndex
// The value is a JSON object containing the fields text, normalisedText,
// nlWordClass, nlEntityClass, nlConfidence, nlEntityId and candidates.
// The normalised text contains the text we will use for referencing;
// the normalisation consists of joining multi word names.
// The nlWordClass, nlEntityClass, nlConfidence and nlEntityId values are added by Nederlab and
// might be used for disambiguation in future.
//
// This scripts populates the candidates field with geoname ids. Candidates are selected by finding
// all the alternate names match the current word.

// the words are not matched if they are in the freqListNotPlace or errorListEval lists that were
// manually created after evaluating the development dataset

const _ = require('highland');
const level = require('level');
// const lev = require('./lev.js');

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});
const put = _.wrapCallback(db.put.bind(db));

const freqListNotPlace = ['Den', 'De', 'In', 'Tent', 'Ik', 'Zee', 'Men', 'Dit', 'Heer', 'Op', 'Vis', 'Is', 'En', 'Man', 'Maar', 'Volk', 'Zo', 'Wind', 'Vorst', 'Oost', 'Hier', 'Van', 'gr', 'Noorden', 'Sultan', 'Bay', 'Myl', 'Land', 'West', 'Na', 'Des', 'Twee', 'Sy', 'Veld', 'Mist', 'Om', 'Sultans', 'Daar', 'Regen', 'Wal', 'Abraham', 'O', 'Eyland', 'Russen', 'Nu', 'Die', 'Stadt', 'Maen', 'Hij', 'Met', 'Hoe', 'Dese', 'Ten', 'Lant', 'Beer', 'Roer', 'Dan', 'Alle', 'Was', 'Ii', 'Sonne', 'Storm', 'Als', 'Heeren', 'Vossen', 'Tenten', 'Mannen', 'Rif', 'Weer', 'U', 'Doe', 'Papen', 'Aan', 'Bier', 'Dus', 'Hun', 'Vaten', 'Toen', 'Haven', 'Zuid', 'Winter', 'Comm', 'Prins', 'Iii', 'Z', 'Dat', 'Onder', 'Kock', 'Zeyl', 'Koy', 'Soon', 'Koran', 'Koude', 'Pera', 'Te', 'Geen', 'Ki', 'Speck', 'Of', 'Er', 'Waar', 'Landen', 'Doch', 'Bergen', 'Gort', 'Ons', 'Zonder', 'Dog', 'Schoon', 'Touwen', 'Ian', 'Wie', 'Mylen', 'Wel', 'Koelte', 'Mast', 'Persing', 'Maer', 'Augustus', 'Klippen', 'Ton', 'Stil', 'Anno', 'Ende', 'Onze', 'Hof', 'Porte', 'Zwarte', 'Pomp', 'Konings', 'Daer', 'Hond', 'Franken', 'Pacha', 'Reis', 'E', 'Honden', 'Noord', 'Tom', 'Groote', 'Uit', 'Marine', 'Dek', 'W', 'Mag', 'Boort', 'Wilde', 'Stuurman', 'Al', 'Zon', 'Kok', 'Koppen', 'Jaar', 'Hemel', 'Alzo', 'Haer', 'Haare', 'Boter', 'Ses', 'Velden', 'Zoon', 'Tot', 'Ed', 'Traen', 'Vat', 'Loots', 'Jaren', 'Baron', 'Christen', 'Eiland', 'Jacob', 'Groot', 'By', 'Soo', 'Robben', 'Gy', 'Nae', 'May', 'Khan', 'Eene', 'St.', 'Zal', 'Plaat', 'Kanaal', 'Zeil', 'Waer', 'Natie', 'Messen', 'Kolen', 'Colonie', 'Alma', 'Lindenau', 'Moer', 'Inden', 'Schapen', 'Son', 'Zie', 'Ja', 'Sijn', 'Mais', 'Vloet', 'Waren', 'Oly', 'Noch', 'So', 'Over', 'Zelve', 'Reven', 'Anker', 'Imam', 'Nauwte', 'Inde', 'Elve', 'Valey', 'Jaer', 'Lens', 'Monster', 'Perekop', 'Compas', 'Archipel', 'Vier', 'Had', 'Pas', 'Kristen', 'Mosul', 'Adel', 'Gansen', 'Boot', 'Oordeel', 'Alles', 'Keizers', 'Divan', 'Silver', 'Huizen', 'Et', 'Granen', 'Staart', 'mal', 'Masten', 'Paarden', 'B', 'Maden', 'Gat', 'Harem', 'Haar', 'Stad', 'Zou', 'Spiegels', 'Steen'];
const errorListEval = ['Rijm', 'Ijder', 'Seven', 'Eerste', 'Kogel', 'Kust', 'Ao', 'Wed', 'Hoeck', 'Johannes', 'Graden', 'Pan', 'Maria', 'Naar', 'Matthieu', 'Poort', 'Dam', 'Karel', 'Uan', 'Mey', 'Kamer', 'Raven'];
const errorListTest = ['Sand', 'Kleeden', 'Dolle', 'Elk', 'Vingen', 'Last', 'Mars', 'Lande', 'Caert', 'Schansen', 'Stads', 'Mai', 'Kraan', 'Saley', 'Iagen', 'Hasen', 'Pael', 'Raas', 'Ly', 'X', 'V', 'Vi', 'Barken', 'I'];

// for all the words
_(db.createReadStream({
  gt: 'txt\x00',
  lt: 'txt\xff',
}))
// find the alternates
.flatMap(({ key, value: {
  text,
  normalisedText,
  nlWordClass,
  nlEntityClass,
  nlConfidence,
  nlEntityId,
} }) => {
// Er is een lijst woorden die zeer veel voorkomen en geen plaatsnaam zijn; uit de frequecy analyse
  if (freqListNotPlace.includes(normalisedText) || errorListEval.includes(normalisedText)
    || errorListTest.includes(normalisedText)) {
    return put(key, {
      text,
      normalisedText,
      nlWordClass,
      nlEntityClass,
      nlConfidence,
      nlEntityId,
      candidates: [],
    });
  }
  return _(db.createValueStream({
    gte: `alt\x00${normalisedText}\x00`,
    lte: `alt\x00${normalisedText}\x00\xff`,
  }))
  .reduce(new Set(), (acc, val) => {
    acc.add(val);
    return acc;
  })
  // copy ids into object
  .map(geoIds => ({
    text,
    normalisedText,
    nlWordClass,
    nlEntityClass,
    nlConfidence,
    nlEntityId,
    candidates: [...geoIds],
  }))
  // store object
  .flatMap(value => put(key, value));
})
.each(() => {})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
