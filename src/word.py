

class Word:
    entity_id = ""
    entity_class = ""
    entity_confidence = ""
    url = ""

    def __init__(self, _id, word, paragraph, paragraphId, sentence, sentenceId, _class):
        self._id = _id
        self.word = word
        self.paragraph = paragraph
        self.paragraphId = paragraphId
        self.sentence = sentence
        self.sentenceId = sentenceId
        self._class = _class
        # self.entity = entity

    def entity(self, _id, _class, confidence):
        self.entity_id = _id
        self.entity_class = _class
        self.entity_confidence = confidence
