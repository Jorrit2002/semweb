// calculates score for place name recognition after runner js/matchGeoIds.js

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});
const annoDb = level('./annotated_db', {
  keyEncoding: 'utf8',
  valueEncoding: 'utf8',
});

const get = _.wrapCallback(db.get.bind(db));

_(annoDb.createReadStream())
.filter(({ key }) => key !== '')
.flatMap(({ key, value }) =>
  get(key)
  .map(({ normalisedText, candidates }) => {
    const tagged = candidates && Array.isArray(candidates) && candidates.length > 0;
    const annotated = value !== 'None';
    if (tagged !== annotated) {
      console.log(`${key} ${value} ${normalisedText} ${candidates}`);
    }
    return [annotated, tagged];
  })
)
.reduce({
  taggedCorrectly: 0,
  totalTagged: 0,
  totalAnnotated: 0,
}, ({ taggedCorrectly, totalTagged, totalAnnotated }, [annotated, tagged]) => {
  let tc = taggedCorrectly;
  let tt = totalTagged;
  let ta = totalAnnotated;
  if (tagged && annotated) {
    tc += 1;
  }
  if (tagged) {
    tt += 1;
  }
  if (annotated) {
    ta += 1;
  }
  return {
    taggedCorrectly: tc,
    totalTagged: tt,
    totalAnnotated: ta,
  };
})
.map(({ taggedCorrectly, totalTagged, totalAnnotated }) => {
  console.log(`${taggedCorrectly} ${totalTagged} ${totalAnnotated}`);
  const precision = taggedCorrectly / totalTagged;
  const recall = taggedCorrectly / totalAnnotated;
  const fscore = (2 * precision * recall) / (precision + recall);
  return ({ precision, recall, fscore });
})
.each((score) => { console.log(score); })
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
