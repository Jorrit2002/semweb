# Linking geographical names in historic Dutch texts to a DBpedia resource using GeoNames as a gazetteer

final project for the masters course on semantic web-technology at the University of Groningen by **Jorrit Bakker, s1857843**, and **Remko Boschker, s1282603**.

## Introduction

Geographical references are an important aspect of historical texts. Annotating a historical text with the correct reference is challenging, because it is time consuming and it requires knowledge about historical names. Annotating automatically is challenging because the spelling of the names can vary greatly, places change name and a name can refer to more than one place.

We wanted to investigate to what extend semantic web-resources can be used to improve automatic referencing of place-names. We chose historical texts because they pose disambiguation challenges that may require some external knowledge to resolve. As the project progressed however it took a lot of time to prepare the corpus and annotations. So we revised our research question to determining if spelling variations in historical texts can be overcome by using a large gazetteer with many alternates and secondly how well we could disambiguate between candidate references using the geographical data available in the gazetteer. We investigated place names in travel descriptions in Dutch from 1600-1800 using [GeoNames][GeoNames] as a gazetteer and referencing to [DBpedia][DBpedia] semantic web resources.

## Prior art

In [Nissim 2004] used an off-the-shelf maximum entropy tagger in order to do named entity recognition in historical descriptions of Scotland. The goal was to recognize historical names in order to make those documents searchable. All locations in a dataset of 648 documents were annotated manually by two annotators with an agreement of 85,82%. In total there were 2357 unique lo- cation instances. The training and evaluation of the Curran and Clark (C&C) was performed by 10-fold cross-validation. The quality of the tagger was eval- uated reporting precision (93.60%), recall (94.87%) and f-score (94.23%). The off-the-shelf maximum entropy tagger can be successfully trained to recognize location names.

Named entity recognition has been established as an important task in NLP. However, a named entity can be referred to by multiple surface forms. A surface form could also refer to multiple entities. In [Cucerza 2007] proposed a disambiguation model to map entities to Wikipedia pages. The disambiguation process uses the context of the surface form and all possible candidate entities to find the best agreement. The documents that should be tagged are analyzed in three steps. First candidate words are selected based on statistics. One of the four entity tags (Location, Person, Organization, Misc) are added to disambiguate between conjunctions. Finally a vector space model is used to disambiguate the entities in the text with the contextual and category information extracted from Wikipedia. The system was evaluated compared to a human annotated test set of Wikipedia articles and to a set of news stories. This approach obtained accuracies of 88% and 91%.

Another approach is taken by [Alex 2015] where the Edinburgh geoparser is adapted for historical text. The geoparser consists of two parts, a geotagging component using nlp techniques to find the geographical names and a georesolution component that uses a gazetteer to lookup the names and resolve ambiguities. The geotagging component does tokenisation and part-of-speech tagging with a third-party tool. It also does rule-based chunking and named entity recognition. The linguistically annotated text is passed to the geoparser that looks up the names in GeoNames and Ordenancy Survey data. It was extended to allow the use of other (historical) gazetteers. Candidate entities are ranked using a variety of heuristics such as a preference for populated places, larger population size, proximity of entities within a text and for ports in text about sea-travel. 

## Preparing the corpus and gazetteer data

We have chosen a corpus of travel descriptions written between 1600 and 1800 taken from the [Nederlab][Nederlab] project corpus. Our corpus contains 11 texts,  984 paragraphs, 3228 sentences and 250830 words. Most describe sea travel, half are about going north towards Greenland, one to Japan, Taiwan and Korea and one trough Turkey towards the Crimea. We obtained the annotated xml source documents courtesy of the [Meertens Institute][Meertens]. A full list is in the references.

We loaded the words in the corpus in a [LevelDB][LevelDB] database with a composite key containing the text name, paragraph, sentence and word indices. The LevelDB database is sorted by key; this makes it easy to step through the texts. Each word entry contains the existing annotation by Nederlab (word class, entity id, entity class, entity label confidence), a normalised text and the orginal text.

The normalised text contains the original text with all-caps changed to small-caps starting with a capital. To compose place names that consist of more than one word (e.g. _Den_, _Haag_ becomes _Den Haag_) we joined entities based on the Nederlab entity id. If the entity ids for the word preceding and following a word are the same we joined them. We inspected the entities whose names we composed and that are tagged as a location by Nederlab. We found that only about one in four of the entities were correctly identified as a composite place name. We also noticed that many of the incorrect compositions involve one of the composites not starting with a capital (mostly _van_). We filtered those out and the composition improved to about half of the compositions being correct.

GeoNames is a geographical database. It contains a lot (over 10 million) of entities with geographical data (coordinates, elevation model, feature class, population, country code, timezone), alternate names (over 5.5 million) and links to [Wikipedia][Wikipedia] pages. It seems to be the ideal gazetteer for the task. All the data is available as a database dump in csv-format. We loaded the GeoNames alternate names that are in Latin script into the database with the name as key and the GeoNames id as value. This allows us to quickly retrieve a GeoNames id if the the alternate name exists. We filtered out alternate names that are postal codes, airport codes, French Revolution names, abbreviations or links to Wikipedia.

We did use the links to Wikipedia in de GeoNames alternate names dataset. We loaded them in the database with the corresponding GeoNames id as key allowing for quick retrieval of a link by GeoNames id. At the same time we converted the Wikipedia url to a DBpedia url by replacing _http(s)://en.wikipedia.org/wiki/[name]_ with _http://dbpedia.org/page/[name]_. We later found that the Dutch Wikipedia pages that were added during annotation did not have a corresponding DBpedia page. We url-encoded any punctuation. We included a mapping in the database going from DBpedia url back to the GeoNames id in case we needed to use information from the GeoNames entity corresponding to a url. No url maps to more than one entity. Not all GeoNames entities have a corresponding url in de dataset. We choose to link to DBpedia resources, because its semantic data format and query end-points could be used for disambiguating between urls (we however did not get to do that). We expected to use the GeoNames geographical data for choosing between entities and added that data to the database as well. We also produced annotation and evaluation databases containing the word id composite as key and the DBpedia url as value.

The basic system is a mapping from a word id to a DBpedia url. Because GeoNames already contains a list of alternate names and links to Wikipedia pages this seems straight-forward as figure 1 shows.

````
word id
: normalise text -> normalised text
: match normalised text with alternate name in db -> GeoNames entity
: retrieve entity link from db -> (word id, DBpedia url)
: evaluate against annotation-db -> f-score
````
**figure 1: basic system layout**

## Annotation

To evaluate our system and guide development we needed a gold standard. The existing annotation of the text did not seem very reliable on manual inspection. We needed to annotate part of the corpus ourselves. We developed a web-application for doing so (see figure 2) and both annotated each text manually for the first 300 words as a development set and the next 600 words as a test set.

![screen annotation](./md/screen-annotation.png)
**figure 2: screenshot of annotation web application used**

For each word starting with a capital letter we decided if the word was a place name and if so selected a link to a Wikipedia page if one was available in the [GeoNames][GeoNames] dataset. If one was not available we searched for one and added it manually. If we could not find a Wikipedia page we marked the word as a place-name with the url ``not found``. If we decided a word was not a place, it was marked as ``None``. Wikipedia urls were automatically converted to DBpedia urls afterwards.

We used Cohen's Kappa to evaluate the annotators agreement. A Kappa greater then 0.75 is considered to be excellent. Having a score between 0.75 and 0.4 means there is a good agreement. A Kappa score below 0.4 means the agreement is poor. The achieved Cohen's Kappa, evaluating the two annotators data, was 0.689. We concluded that there is a good agreement. The occurrence of place names appears to be concentrated at the beginning of the text as can be seen in table 1. The results will show that more annotation in the test set would have been better, because the result is now very sensitive to a few alternate names missing from the GeoNames dataset.

**table 1: annotation counts**
| annotated set | words in each text | starts with capital | None | Not found | url |
| --- | --- | --- | --- | --- | --- |
| development | 0 - 300 | 725 | 641 | 9 | 75 |
| test | 300 - 900 | 856 | 836 | 3 | 17 |

In discussing the annotation and doing some background research we found that for some places we could find a valid reference afteral. This was the case for _Staatenhoek_, _Maurits Bay_, _Naeuwte Hudsons_ and _Freto Hudsons_. For other places we discussed what DBpedia page to link to, for instance should _Coeree_ link to _Korea_ or _South Korea_? We choose the most recent and geographically accurate entity. The result of this is that for instance _Constantinopolen_ refers to the page about _Istanbul_ and not _Constantinople_. We discussed *Oost-indienden, Korea, Constantinopolen, Spitsberghen, Holland, Jejudo* in this way. We ignored what we believe to be an OCR error _Diemaath_. And there were two streets in Amsterdam (_Nieuwe straet, Kleine Vismarkt_) and four other places (_Tyocen, Pousaan, Christiaens-streat, Monnicke-Nes_) we did not manage to annotate. We changed our annotations accordingly.

We use these annotations to evaluate the references. We evaluate on precision, recall and f-score. The precision is the ``number recognised correctly / number recognised``. The recall is the ``number recognised correctly / number of place-names in annotation``. The f-score is calculated as ``2 * (precision * recall) / (precision + recall)``. The development set is used for improving the geographical entity recognition. The test set is used to measure the performance of the task of automatically referencing place-names to DBpedia url's in a historical text.

## Geographical name recognition

We developed a basic geographical name recognition system by checking if words that start with a capital are identical to one of the alternate names in the GeoNames dataset. We tried matching words with an edit distance of one or two, but the number of possible matches became very large and performance was very poor. Although geographical name recognition did not initially have our focus it turns out that it is an important part of disambiguation, because many Dutch non-name words actually match a GeoNames alternate name.

To limit matching non place-names we created a black-list of the top 250 words in the development set that matched with an alternate name but are not a place-name in the text sorted in descending order by the number of times they occur. We manually removed 49 place-names from the top 299 words to obtain it. To improve matching on place-names that are not in the GeoNames alternate names list, we have added names that were not in it to our database as we encountered them. The alternate _Haarlemmerdyk_ for _Haarlemmerdijk_ could not be added, because there appears to be no corresponding GeoNames entity. Both the black-list and added alternates are listed at the end of this text.

Table 2 shows the scores for the geographical name recognition evaluated on the annotated development dataset for using all words with a capital, for the annotation provided by Nederlab and for matching with GeoNames alternate names and with the frequent error filter and with the missing alternates added. Lastly we included the score for a black-list tailored to filter out the errors encountered in the development dataset. This last result will most likely only improve scores for the development set. We use the system with all the filters and alternates added to generate candidate entities for possible a reference.

**table 2: scores for geographical name recognition in the development set**
| system | precision | recall |  f-score |
| --- | --- | --- | --- |
| all capitals | 0.116 | 1.000 | 0.108 |
| Nederlab | 0.258 | 0.559 | 0.353 |
| alt names | 0.181 | 0.429 | 0.254|
| + black-list | 0.537 | 0.429 | 0.477|
| + alternates | 0.676 | 0.821 | 0.742 |
| + specific | 0.919 | 0.809 | 0.861 |


## Selecting GeoNames entity

The basic system outlined in figure 1 does not take into account that a word can match multiple GeoNames alternate names and may refer to more than one GeoNames entity.  Each GeoNames entity include fields for latitude, longitude, feature class, feature code, country code and population to help select between the entities. The actual system used can be summarised by figure 3.

````
word id
: normalise text -> normalised text
: match normalised text with alternate names in db -> GeoNames entities
: add url from db to entities -> entities with url
: use entity geographical information to sort them -> sorted entities
: pick the first from the sorted entities, return url ->  (word id, url)
: evaluate against annotation-db -> f-score
````
**figure 3: layout of the system used**

We expected the feature class would be a good indicator of relevance preferring Hypsographic features. These are features of elevation such as bay, coast, narrow, strait, island. Another good candidate would be Hydrographic features referring to water (sea, canal, river). Both of these should feature frequently in sea travel descriptions. We counted the feature classes of the entities corresponding to the url's in the development annotated dataset.

Five out of seventy-five urls were added manually during annotation and did not have a mapping to a GeoNames entity in the database (Nieuwe_Brug_(Amsterdam) 2x, Altona_(Hamburg), Vlieter, Haarlemmerdijk). They were not included in the following count: Administrative Boundary Features 36, Populated Place Features 20, Hypsographic Features 7, Hydrographic Features 6, Spot Features 1. Administrative and populated place features turn out to be more important and we have sorted the candidate entities accordingly. The feature classes are subdivided in feature codes. They can be found at [http://www.geonames.org/export/codes.html][geo feature codes]. We also counted the feature codes of the entities selected in the annotated development set and sorted accordingly. The counts are: PCLI 14, PCLD 12, ADM1 8, PPLA 7, PPL 6, ISL 4, STRT 4, CAPE 3, PPLL 2, BAY 2, ADM2 2, PPLA3 1, FCL 1.

We evaluated the DBpedia url selected for each word in the corpus with regards to the development and test annotated datasets. We evaluated a naive system picking the first candidate of an unsorted set, a system sorting on the availability of an url, a system sorting on both url and feature class (sorting according to the counts found for each class), a system sorting on both url and feature code and a system sorting first on url, then the feature class and lastly on feature code. Table 3 shows the results for the development set and tables 4 and 5 for the test set.

**table 3: evaluation scores for the different systems with regards to the development set**
| system | precision | recall |  f-score |
| --- | --- | --- | --- |
| naive | 0.913 | 0.494 | 0.641 |
| url | 0.926 | 0.588 | 0.719 |
| class | 0.937 | 0.694 | 0.797 |
| code | 0.931 | 0.635 | 0.755 |
| all | 0.934 | 0.671 | 0.781 |

The best score is achieved sorting by the feature class. Only in two out of twenty six incorrectly linked place-names was the wrong candidate chosen and thus posing an actual disambiguation question. In two cases the composition of name parts was incorrect. The other cases had to do with the url not being in the GeoNames database or we did not add the alternate name because we did not know what the place-name referred to. Only four words were incorrectly recognised as place-names. They were all sailers with last names that are identical to place-names: van Brugge, van Hull, van Gottenburg, van London.

**table 4: evaluation scores for the different systems with regards to the test set**
| system | precision | recall |  f-score |
| --- | --- | --- | --- |
| naive | 0.108 | 0.200 | 0.140 |
| url | 0.175 | 0.350 | 0.233 |
| class | 0.266 | 0.600 | 0.369 |
| code | 0.195 | 0.400 | 0.262 |
| all | 0.267 | 0.600 | 0.369 |

The scores are very low. We are only evaluating on 20 values and single errors have a large impact. Next we applied the same method for improving results as we did for the geographical name recognition evaluated on the development data set. We are aware that tuning results based on the test set does not give a reliable indication of performance, but we wanted to see if there are similar improvements.

We noticed that six names were unknown (_Sinope, Hit-land, Nauwte Hudsons, Galamkes, Christiaens-straet, Monnicke-Nes_). We were not able to find what the last two refer to, but the others were added to the alternate names list. After adding the alternate names the place-names that are missed are _Christiaens-straet_, _Monnicke-Nes_ and _Hudsons_. The first two are unknown to us and the last one is an incorrect composition. It should be _Nauwte Hudsons_. The precision is very low and looking at the incorrectly recognised words we see that there are four sailors with a last name that is close to a place-name (_Bremer_, _Gottenburg_, _Spitsbergen_, _Dronten_) and an adjective mistaken for a noun _France Wyn_. The other words were added to another black-list to produce the results in table 5. The list is included as the _Specific black-list after evaluation of test set_ at the end of this text.

**table 5: evaluation scores for the different systems with regards to the test set, missing alternate names and black-list added**
| system | precision | recall |  f-score |
| --- | --- | --- | --- |
| naive | 0.600 | 0.450 | 0.514 |
| url | 0.667 | 0.600 | 0.632 |
| class | 0.739 | 0.850 | 0.791 |
| code | 0.684 | 0.650 | 0.667 |
| all | 0.739 | 0.850 | 0.791 |

Sorting by url availability and feature class consistently produces the best result. Adding alternate names and words to the black-list is reliable method for improving the scores.

## Discussion and future work

All in all the system works but has a large manual component to it. It seems we can keep adding alternate names and words to the black-list to improve results. But even with adding all the known alternate names and black-listing all errors place-names being used for something else such as a last name and not knowing what a entity a historic name refers to are still issues. The last issue might be resolved by an expert in historical names. We might also improve the current system further by annotating more and evaluating on a larger test set. Disambiguation does not seem the be a large issue, but we could still try to improve results using the population size or proximity of entities within a text.

As far as we can see there are two main lines of alternate approach. One consists of annotating more text and training an entity recognizer performing well, such as [Nissim 2004], to avoid the black-lists. The Edinburgh geoparser system [Grover 2014, Alex 2015] works well without training for historical English. But it uses many lists similar to the manual black-lists and uses GeoNames alternate names as well and so it is dependent on most of the names being present. Perhaps a way can be found to add Dutch historical geographical names from other resources to GeoNames automatically or to use these other sources directly to improve recall. It might further be improved by searching for names in DBpedia directly.

The second approach improves recall by matching more permissively for instance by allowing an Levenshtein distance of one or two for longer words. This would generate many more matches not only for actual geographical names, but also for other words. We would then need some sophisticated disambiguation step that includes ``None`` as one of the options. Such a disambiguation step might use extra information from a semantic web resource such as DBpedia examples of such an approach can be found in [Cucerzan 2007, Kazama 2007, Hachey 2013].

## Conclusion

Spelling variation in historical texts can be overcome by using a large gazetteer. However in the approach we took the gazetteer needs to contain the variations. This simply displaces the problem and does not really solve it. In the discussion we make suggestions for a more automated solution. We can disambiguate between candidate entities using their GeoNames feature class reliably. But we avoid having to disambiguate between candidate entities and no candidate (not a place-name) by filtering out common errors with a manually composed blacklist. In the spirit of texts in the corpus used this project was more of an exploratory journey than a quick commute.

## References

### Academic papers

* Alex, B., Byrne, K., Grover, C., & Tobin, R. (2015). Adapting the edinburgh geoparser for historical georeferencing. International Journal of Humanities and Arts Computing, 9(1), 15-35.
* Cucerzan, S. (2007). Large-scale named entity disambiguation based on wikipedia data. Emnlp-Conll, , 7. pp. 708-716.
* Grover, C., & Tobin, R. (2014). A gazetteer and georeferencing for historical english documents. Proceedings of the EACL LaTeCH Workshop, pp. 119-127.
* Hachey, B., Radford, W., Nothman, J., Honnibal, M., & Curran, J. R. (2013). Evaluating entity linking with wikipedia. Artificial Intelligence, 194, 130-150.
* Kazama, J., & Torisawa, K. (2007). Exploiting wikipedia as external knowledge for named entity recognition. Proceedings of the 2007 Joint Conference on Empirical Methods in Natural Language Processing and Computational Natural Language Learning (EMNLP-CoNLL), pp. 698-707.
* Nissim, M., Matheson, C., & Reid, J. (2004). Recognising geographical entities in scottish historical documents. Proceedings of the Workshop on Geographic Information Retrieval at SIGIR 2004,
* Tjong Kim Sang, Erik F, & De Meulder, F. (2003). Introduction to the CoNLL-2003 shared task: Language-independent named entity recognition. Proceedings of the Seventh Conference on Natural Language Learning at HLT-NAACL 2003-Volume 4, pp. 142-147.

### Online resources

[Wikipedia]: https://en.wikipedia.org/wiki/Main_Page
[DBpedia]: http://wiki.dbpedia.org/
[GeoNames]: http://www.GeoNames.org/
[LevelDB]: https://github.com/google/leveldb
[Nederlab]: http://www.nederlab.nl/
[Meertens]: https://www.meertens.knaw.nl/cms/en/
[geo feature codes]: http://www.geonames.org/export/codes.html

* Wikipedia: [https://en.wikipedia.org/wiki/Main_Page](https://en.wikipedia.org/wiki/Main_Page)
* DBpedia: [http://wiki.dbpedia.org/](http://wiki.dbpedia.org/)
* GeoNames: [http://www.GeoNames.org/](http://www.GeoNames.org/)
* LevelDB: [https://github.com/google/leveldb](https://github.com/google/leveldb)
* Project repository: [https://bitbucket.org/Jorrit2002/semweb](https://bitbucket.org/Jorrit2002/semweb) please see the readme for further details

### Corpus

#### Query [Nederlab][Nederlab] corpus

__title:__ alle collections, metadata van bron: genre: reisbeschrijving, samenstelling: zelfstandige titles, onzelfstandige titles, koepeltitles, herdrukken uitgesloten

__description:__ alle collections, metadata van bron: genre: reisbeschrijving, samenstelling: zelfstandige titles, onzelfstandige titles, koepeltitles, herdrukken uitgesloten

__search query:__ alle collections, metadata van bron: genre: reisbeschrijving, publicatieperiode: 1600-1800, samenstelling: zelfstandige titles, onzelfstandige titles, koepeltitles, herdrukken uitgesloten, tekst beschikbaar

__last changed:__	2016-10-04 10:01:05

__created at:__	2016-10-04 09:54:33

#### Title list

__title:__ _Drie Uoyagien Gedaen na Groenlandt, Om te ondersoecken of men door de Naeuwte Hudsons soude konnen Seylen; om alsoo een Doorvaert na Oost-Indien te vinden_
__dated:__ ca. 1660-1670
__author:__ Chr. van Sichem (Amsterdam (Noord-Holland), 1642-ca. 1693-1698) Jens Munk (Arendal, 1579-Kopenhagen, 1628) Martin Frobisher (Normanton, ca. 1535-Plymouth, 1594) Godske Lindenau (?(16de eeuw)-Kopenhagen, 1612)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Journael, of dagh-register, gehouden by seven matroosen, in haer overwinteren op Spitsbergen in Maurits-bay, gelegen in Groenlandt: t’zedert het vertreck van de visschery-schepen der geoctroyeerde Noordtsche Compagnie, in Nederlandt, zijnde den 30. Augusty, 1633. tot de wederkomst der voorsz. schepen, den 27. May, Anno 1634_
__dated:__ ca. 1663-1665
__author:__ Jacob Segersz van der Brugge (ca. 1580-1610-ca. 1634-1700)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Journael van de ongeluckighe voyagie, gedaen bij den commandeur Dirck Albertsz. Raven, naer Groenlandt, in den iare 1639_
__dated:__ ca. 1665
__author:__ Dirck Albertsz Raven (Hoorn (Noord-Holland), ca. 1589-1590-Hoorn (Noord-Holland), ca. 1640-1670)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Twee iournalen, het eerste gehouden by de seven matroosen, op het eylandt Mauritius, in Groenlandt, in den iare 1633. en 1634. in haer overwinteren, doch sijn al tsamen gestorven: En het tweede gehouden by de seven matroosen, die op Spitsbergen zijn overwintert, en aldaer ghestorven, in den iare 1634. Verhalende de wonderheden van de beeren, walvisschen, onlydelijcke koude, storm-winden en lange nachten, die zy hebben geleden._
__dated:__ ca. 1665
__author:__ anoniem Twee iournalen, het eerste gehouden by de seven matroosen, op het eylandt Mauritius, in Groenlandt (-)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Journael van de ongeluckige voyagie van t jacht de Sperwer van Batavia gedestineert na Tayowan in t jaar 1653, en van daar op Japan, gevolgd door Beschryvinge van t Koninghrijck Coeree_
__dated:__ 1668
__author:__ Hendrick Hamel (Gorinchem (Zuid-Holland), 1630-Gorinchem (Zuid-Holland), 1692)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Kort en opregt verhaal van het droevig en avontuurlijk wedervaren van Abraham Jansz. van Oelen_
__dated:__ 1683
__author:__ anoniem Kort en opregt verhaal van het droevig en avontuurlijk wedervaren van Abraham Jansz. van Oelen (-)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Het Journaal en Daghregister van Dirk Jacobsz. Tayses Avontuurelyke Reyse na Groenlandt, gedaen met het Schip Den Dam, in ’t Jaar 1710_
__dated:__ 1711
__author:__ Willem Pieterse Poort (?(17de eeuw)-?(18de eeuw))
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Verhaal der Merkwaardige Reize van den Kommandeur Jakob Jansen_
__dated:__ 1770
__author:__ Jakob Jansen (?(18de eeuw)-)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Beknopt en getrouw verhaal, van de reys van commandeur Jeldert Jansz Groot, uit Texel na en in Groenland &c. Deszelfs verblyf op de kust van Oud-Groenland, naa het verongelukken van deszelfs onderhebbend schip, tussen Ysland en Staatenhoek_
__dated:__ ca. 1779
__author:__ Jeldert Jansz. Groot (Schiermonnikoog (Friesland), 1739-na 1788)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Omstandig journaal van de reize naar Groenland, gedaan door commandeur Maarten Mooy, met het schip Frankendaal_
__dated:__ 1787
__author:__ Maarten Mooy (1739-1817)
__genre:__ non-fictie, reisbeschrijving
__collection:__ DBNL

__title:__ _Aanteekeningen, gehouden op eene reize door Turkeyen, Natoliën, de Krim en Rusland in de jaren 1784-89_
__dated:__ 1792-1795
__author:__ Pieter van Woensel (Haarlem (Noord-Holland), 1747-Den Haag (Zuid-Holland), 1808)
__genre:__ fictie, non-fictie, proza, reisbeschrijving
__collection:__ DBNL

## Black-listed words in descending order of frequency

Den, De, In, Tent, Ik, Zee, Men, Dit, Heer, Op, Vis, Is, En, Man, Maar, Volk, Zo, Wind, Vorst, Oost, Hier, Van, gr, Noorden, Sultan, Bay, Myl, Land, West, Na, Des, Twee, Sy, Veld, Mist, Om, Sultans, Daar, Regen, Wal, Abraham, O, Eyland, Russen, Nu, Die, Stadt, Maen, Hij, Met, Hoe, Dese, Ten, Lant, Beer, Roer, Dan, Alle, Was, Ii, Sonne, Storm, Als, Heeren, Vossen, Tenten, Mannen, Rif, Weer, U, Doe, Papen, Aan, Bier, Dus, Hun, Vaten, Toen, Haven, Zuid, Winter, Comm, Prins, Iii, Z, Dat, Onder, Kock, Zeyl, Koy, Soon, Koran, Koude, Pera, Te, Geen, Ki, Speck, Of, Er, Waar, Landen, Doch, Bergen, Gort, Ons, Zonder, Dog, Schoon, Touwen, Ian, Wie, Mylen, Wel, Koelte, Mast, Persing, Maer, Augustus, Klippen, Ton, Stil, Anno, Ende, Onze, Hof, Porte, Zwarte, Pomp, Konings, Daer, Hond, Franken, Pacha, Reis, E, Honden, Noord, Tom, Groote, Uit, Marine, Dek, W, Mag, Boort, Wilde, Stuurman, Al, Zon, Kok, Koppen, Jaar, Hemel, Alzo, Haer, Haare, Boter, Ses, Velden, Zoon, Tot, Ed, Traen, Vat, Loots, Jaren, Baron, Christen, Eiland, Jacob, Groot, By, Soo, Robben, Gy, Nae, May, Khan, Eene, St., Zal, Plaat, Kanaal, Zeil, Waer, Natie, Messen, Kolen, Colonie, Alma, Lindenau, Moer, Inden, Schapen, Son, Zie, Ja, Sijn, Mais, Vloet, Waren, Oly, Noch, So, Over, Zelve, Reven, Anker, Imam, Nauwte, Inde, Elve, Valey, Jaer, Lens, Monster, Perekop, Compas, Archipel, Vier, Had, Pas, Kristen, Mosul, Adel, Gansen, Boot, Oordeel, Alles, Keizers, Divan, Silver, Huizen, Et, Granen, Staart, mal, Masten, Paarden, B, Maden, Gat, Harem, Haar, Stad, Zou, Spiegels, Steen

## Alternate names added to the dataset

| alt name | name | GeoNames Id |
| --- | --- | --- |
| St. Anna Land | Sint-Annaland | 2747298 |
| Anna Land | Sint-Annaland | 2747298 |
| Groen-Land | Greenland | 3425505 |
| Maurits-Bay | Maurits Baai | 1631580 |
| Staatenhoek | Cape Farewell | 3423843 |
| Groen-Land | Greenland | 3425505 |
| Vlieter | Vlieter | 2745405 |
| Hitlandt | Shetland | 8299621 |
| Coeree | South Korea | 1835841 |
| Osaacke | Osaka | 1853909 |
| Quelpaarts Eylandt | Jeju-do | 1846265 |
| Nankijn | Nanjing | 1799962 |
| Cux | Cuxhaven | 2939658 |
| Hitland | Shetland | 8299621 |
| Oost-Indien | Republic of Indonesia | 1643084 |
| Naeuwte Hudsons | Hudson Strait | 6640370 |
| Denemarcken | Denmark | 2623032 |
| Capo Farwel | Cape Farewell | 3423843 |
| Freto Hudsons | Hudson Strait | 6640370 |
| Galehamkes Landt | Gael Hamke Bugt | 3423742 |
| Groenlandt | Greenland | 3425505 |
| Spitsberghen | Spitsbergen | 7535696 |
| Voorlandt | Prins Karls Forland | 2729238 |
| Constantinopolen | Istanbul | 745044 |
| Nederlandt | Netherlands | 2750405 |
| Hollandt | Netherlands | 2750405 |
| Yslandt | Iceland | 2629691 |
| Chyna | China | 1814991 |
| Sinope | Sinop | 739600 |
| Hit-land | Shetland | 8299621 |
| Nauwte Hudsons | Hudson Strait | 6640370 |
| Galamkes | Gael Hamke Bugt | 3423742 |

## Specific black-list after evaluation of development set

Rijm, Ijder, Seven, Eerste, Kogel, Kust, Ao, Wed, Hoeck, Johannes, Graden, Pan, Maria, Naar, Matthieu, Poort, Dam, Karel, Uan, Mey, Kamer, Raven

## Specific black-list after evaluation of test set

Sand, Kleeden, Dolle, Elk, Vingen, Last, Mars, Lande, Caert, Schansen, Stads, Mai, Kraan, Saley, Iagen, Hasen, Pael, Raas, Ly, X, V, Vi, Barken, I
