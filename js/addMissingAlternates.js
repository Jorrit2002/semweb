// adds alternate names not available in the GeoNames alternate names database

/* after evaluating development set
| St. Anna Land | Sint-Annaland | 2747298 |
| Anna Land | Sint-Annaland | 2747298 |
| Groen-Land | Greenland | 3425505 |
| Nieuwe-straet | Nieuwe Straat |  ? |
| Maurits-Bay | Maurits Baai | 1631580 |
| Staatenhoek | Cape Farewell | 3423843 |
| Groen-Land | Greenland | 3425505 |
| Haarlemmerdyk | Haarlemmerdijk | ? |
| Vlieter | Vlieter | 2745405 |
| Hitlandt | Shetland | 8299621 |
| Coeree | South Korea | 1835841 |
| Osaacke | Osaka | 1853909 |
| Quelpaarts Eylandt | Jeju-do | 1846265 |
| Nankijn | Nanjing | 1799962 |
| Cux | Cuxhaven | 2939658 |
| Hitland | Shetland | 8299621 |
| Oost-Indien | Republic of Indonesia | 1643084 |
| Naeuwte Hudsons | Hudson Strait | 6640370 |
| Tyocen | | |
| Pousaan | | |
| Denemarcken | Denmark | 2623032 |
| Capo Farwel | Cape Farewell | 3423843 |
| Freto Hudsons | Hudson Strait | 6640370 |
| Galehamkes Landt | Gael Hamke Bugt | 3423742 |
| Groenlandt | Greenland | 3425505 |
| Spitsberghen | Spitsbergen | 7535696 |
| Voorlandt | Prins Karls Forland | 2729238 |
| Constantinopolen | Istanbul | 745044 |
| Nederlandt | Netherlands | 2750405 |
| Hollandt | Netherlands | 2750405 |
| Yslandt | Iceland | 2629691 |
| Chyna | China | 1814991 |
*/

/* after evalutating test set
['Sinope', 739600],
['Hit-land', 8299621],
['Nauwte Hudsons', 6640370],
['Galamkes', 3423742],
*/


const _ = require('highland');
const level = require('level');

const db = level('./db', {
  valueEncoding: 'json',
});
const put = _.wrapCallback(db.put.bind(db));

const newAlts = [
  ['St. Anna Land', 2747298],
  ['Anna Land', 2747298],
  ['Groen-Land', 3425505],
  ['Maurits-Bay', 1631580],
  ['Staatenhoek', 3423843],
  ['Groen-Land', 3425505],
  ['Vlieter', 2745405],
  ['Hitlandt', 8299621],
  ['Coeree', 1835841],
  ['Osaacke', 1853909],
  ['Quelpaarts Eylandt', 1846265],
  ['Nankijn', 1799962],
  ['Cux', 2939658],
  ['Hitland', 8299621],
  ['Oost-Indien', 1643084],
  ['Naeuwte Hudsons', 6640370],
  ['Denemarcken', 2623032],
  ['Capo Farwel', 3423843],
  ['Freto Hudsons', 6640370],
  ['Galehamkes Landt', 3423742],
  ['Groenlandt', 3425505],
  ['Spitsberghen', 7535696],
  ['Voorlandt', 2729238],
  ['Constantinopolen', 745044],
  ['Nederlandt', 2750405],
  ['Hollandt', 2750405],
  ['Yslandt', 2629691],
  ['Chyna', 1814991],
  ['Sinope', 739600],
  ['Hit-land', 8299621],
  ['Nauwte Hudsons', 6640370],
  ['Galamkes', 3423742],
];

_(newAlts)
.flatMap(([name, id]) => put(`alt\x00${name}\x00nl`, id))
.errors((err) => {
  console.log(err);
})
.each(() => {
  // process.stdout.write('.');
})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
