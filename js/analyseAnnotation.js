// prints links with list of normalised texts that we annotated or matched for it
// in a database of format txt\x00wordId url (annotation_db or evaluation_db)

const _ = require('highland');
const level = require('level');

const remkoDb = level('./annotated_db', {
  valueEncoding: 'utf8',
});

const db = level('./db', {
  valueEncoding: 'json',
});

const get = _.wrapCallback(db.get.bind(db));

_(remkoDb.createReadStream({
  gte: 'txt\x00',
  lte: 'txt\xff',
}))
.filter(({ value }) => value !== 'None')
// replace word id with normalisedText
.flatMap(({ key, value }) =>
  get(key)
  .map(({ normalisedText }) => ({ key: normalisedText, value }))
)
.reduce(new Map(), (acc, { key, value }) => {
  // and value are reversed
  // link will become key and word-keys the values in the list
  if (acc.has(value)) {
    acc.set(value, acc.get(value).concat(key));
  } else {
    acc.set(value, [key]);
  }
  return acc;
})
.errors((err) => { console.log(err); })
.each((map) => {
  map.forEach((val, key) => { console.log(`${key}\t${val}`); });
})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
