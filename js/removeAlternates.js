// removes alternate names from the database

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  valueEncoding: 'json',
});
const del = _.wrapCallback(db.del.bind(db));

const remove = [
  ['Sinope', 739600],
  ['Hit-land', 8299621],
  ['Nauwte Hudsons', 6640370],
  ['Galamkes', 3423742],
];

_(remove)
.flatMap(([name]) => del(`alt\x00${name}\x00nl`))
.errors((err) => {
  console.log(err);
})
.each(() => {
  // process.stdout.write('.');
})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
