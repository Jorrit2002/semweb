# corpus

## kenmerken corpus nederlab

__titel:__ alle collecties, metadata van bron: genre: reisbeschrijving, samenstelling: zelfstandige titels, onzelfstandige titels, koepeltitels, herdrukken uitgesloten

__omschrijving:__ alle collecties, metadata van bron: genre: reisbeschrijving, samenstelling: zelfstandige titels, onzelfstandige titels, koepeltitels, herdrukken uitgesloten

__zoekopdracht:__ alle collecties, metadata van bron: genre: reisbeschrijving, publicatieperiode: 1600-1800, samenstelling: zelfstandige titels, onzelfstandige titels, koepeltitels, herdrukken uitgesloten, tekst beschikbaar

__laatste wijziging op__	2016-10-04 10:01:05

__aangemaakt op__	2016-10-04 09:54:33

## titel lijst

__titel:__ _Drie Uoyagien Gedaen na Groenlandt, Om te ondersoecken of men door de Naeuwte Hudsons soude konnen Seylen; om alsoo een Doorvaert na Oost-Indien te vinden_
__datering:__ ca. 1660-1670
__auteur:__ Chr. van Sichem (Amsterdam (Noord-Holland), 1642-ca. 1693-1698) Jens Munk (Arendal, 1579-Kopenhagen, 1628) Martin Frobisher (Normanton, ca. 1535-Plymouth, 1594) Godske Lindenau (?(16de eeuw)-Kopenhagen, 1612)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Journael, of dagh-register, gehouden by seven matroosen, in haer overwinteren op Spitsbergen in Maurits-bay, gelegen in Groenlandt: t’zedert het vertreck van de visschery-schepen der geoctroyeerde Noordtsche Compagnie, in Nederlandt, zijnde den 30. Augusty, 1633. tot de wederkomst der voorsz. schepen, den 27. May, Anno 1634_
__datering:__ ca. 1663-1665
__auteur:__ Jacob Segersz van der Brugge (ca. 1580-1610-ca. 1634-1700)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Journael van de ongeluckighe voyagie, gedaen bij den commandeur Dirck Albertsz. Raven, naer Groenlandt, in den iare 1639_
__datering:__ ca. 1665
__auteur:__ Dirck Albertsz Raven (Hoorn (Noord-Holland), ca. 1589-1590-Hoorn (Noord-Holland), ca. 1640-1670)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Twee iournalen, het eerste gehouden by de seven matroosen, op het eylandt Mauritius, in Groenlandt, in den iare 1633. en 1634. in haer overwinteren, doch sijn al t'samen gestorven: En het tweede gehouden by de seven matroosen, die op Spitsbergen zijn overwintert, en aldaer ghestorven, in den iare 1634. Verhalende de wonderheden van de beeren, walvisschen, onlydelijcke koude, storm-winden en lange nachten, die zy hebben geleden._
__datering:__ ca. 1665
__auteur:__ anoniem Twee iournalen, het eerste gehouden by de seven matroosen, op het eylandt Mauritius, in Groenlandt (-)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Journael van de ongeluckige voyagie van 't jacht de Sperwer van Batavia gedestineert na Tayowan in 't jaar 1653, en van daar op Japan, gevolgd door Beschryvinge van 't Koninghrijck Coeree_
__datering:__ 1668
__auteur:__ Hendrick Hamel (Gorinchem (Zuid-Holland), 1630-Gorinchem (Zuid-Holland), 1692)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Kort en opregt verhaal van het droevig en avontuurlijk wedervaren van Abraham Jansz. van Oelen_
__datering:__ 1683
__auteur:__ anoniem Kort en opregt verhaal van het droevig en avontuurlijk wedervaren van Abraham Jansz. van Oelen (-)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Het Journaal en Daghregister van Dirk Jacobsz. Tayses Avontuurelyke Reyse na Groenlandt, gedaen met het Schip Den Dam, in ’t Jaar 1710_
__datering:__ 1711
__auteur:__ Willem Pieterse Poort (?(17de eeuw)-?(18de eeuw))
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Verhaal der Merkwaardige Reize van den Kommandeur Jakob Jansen_
__datering:__ 1770
__auteur:__ Jakob Jansen (?(18de eeuw)-)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Beknopt en getrouw verhaal, van de reys van commandeur Jeldert Jansz Groot, uit Texel na en in Groenland &c. Deszelfs verblyf op de kust van Oud-Groenland, naa het verongelukken van deszelfs onderhebbend schip, tussen Ysland en Staatenhoek_
__datering:__ ca. 1779
__auteur:__ Jeldert Jansz. Groot (Schiermonnikoog (Friesland), 1739-na 1788)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Omstandig journaal van de reize naar Groenland, gedaan door commandeur Maarten Mooy, met het schip Frankendaal_
__datering:__ 1787
__auteur:__ Maarten Mooy (1739-1817)
__genre:__ non-fictie, reisbeschrijving
__collectie:__ DBNL

__titel:__ _Aanteekeningen, gehouden op eene reize door Turkeyen, Natoliën, de Krim en Rusland in de jaren 1784-89_
__datering:__ 1792-1795
__auteur:__ Pieter van Woensel (Haarlem (Noord-Holland), 1747-Den Haag (Zuid-Holland), 1808)
__genre:__ fictie, non-fictie, proza, reisbeschrijving
__collectie:__ DBNL
