// prints all the words in the text; two line-breaks after paragraph, space after word

const fs = require('fs');
const _ = require('highland');
const level = require('level');

const db = level('./db', {
  valueEncoding: 'json',
});

_(db.createReadStream({
  gte: 'txt\x00',
  lte: 'txt\xff',
}))
.map(({ key, value: { normalisedText } }) => [key.split('\x00'), normalisedText])
// 'txt', '_kor010kort01_01', 'TEI', '2','text','body','div','p','335','s','5','w','18'
.map(([[, , , , , , , , paragraph], txt]) => [paragraph, txt])
.through((s) => {
  let previousParagraph = null;

  return s.consume((err, x, push, next) => {
    if (err) {
      // pass errors along the stream and consume next value
      push(err);
      next();
    } else if (x === _.nil) {
      // pass nil (end event) along the stream
      push(null, x);
    } else {
      const [paragraph, txt] = x;
      if (paragraph !== previousParagraph) {
        previousParagraph = paragraph;
        push(null, '\n\n');
      }
      push(null, txt);
      push(null, ' ');
      next();
    }
  });
})
.pipe(fs.createWriteStream('allText.txt'));
