// print counts for words that have beeen tagged as a location by Nederlab
// sorted by count and showing Nederlab entity classification

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});


// { key, value: {
//   text,
//   normalisedText,
//   nlWordClass,
//   nlEntityClass,
//   nlEntityConfidence,
//   nlEntityId,
//   candidates [geoIds]
// }

// for all the words
_(db.createReadStream({
  gt: 'txt\x00',
  lt: 'txt\xff',
}))
// find the alternates
.filter(({ value: { nlEntityClass } }) => nlEntityClass === 'loc')
.group(({ value: { normalisedText } }) => normalisedText)
.flatMap(groups =>
  _(Object.keys(groups))
  .map((key) => {
    const freq = groups[key].length;
    const avgConf = groups[key].reduce((acc, { value }) => {
      const conf = value.nlEntityConfidence ? parseFloat(value.nlEntityConfidence) : 0;
      return acc + conf;
    }, 0) / freq;
    return [key, freq, avgConf];
  })
)
.sortBy((a, b) => b[1] - a[1])
.tap(([key, freq, avgConf]) => console.log(` ${freq}\t${avgConf}\t${key}`))
.reduce(0, (acc, [, freq]) => acc + freq)
.each((tot) => { console.log(`total ${tot}`); })
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
