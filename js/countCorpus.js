// prints the number of words, sentences and paragraphs in the corpus

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  valueEncoding: 'json',
});

_(db.createReadStream({
  gte: 'txt\x00',
  lte: 'txt\xff',
}))
.map(({ key, value: { nlEntityClass } }) => [key.split('\x00'), nlEntityClass])
// filter punctuation
.filter(([, entityClass]) => entityClass !== 'PUNCTUATION')
// 'txt', '_kor010kort01_01', 'TEI', '2','text','body','div','p','335','s','5','w','18'
.map(([[, , , , , , , , paragraph, , sentence, , word]]) => [paragraph, sentence, word])
.through((s) => {
  let previousParagraph = null;
  let previousSentence = null;
  let paragraphCnt = 0;
  let sentenceCnt = 0;
  let wordCnt = 0;

  return s.consume((err, x, push, next) => {
    if (err) {
      // pass errors along the stream and consume next value
      push(err);
      next();
    } else if (x === _.nil) {
      push(null, [paragraphCnt, sentenceCnt, wordCnt]);
      // pass nil (end event) along the stream
      push(null, x);
    } else {
      const [paragraph, sentence] = x;
      if (paragraph !== previousParagraph) {
        paragraphCnt += 1;
        previousParagraph = paragraph;
      }
      if (sentence !== previousSentence) {
        sentenceCnt += 1;
        previousSentence = sentence;
      }
      wordCnt += 1;
      next();
    }
  });
})
.errors((err) => {
  console.log(err);
})
.each((cnts) => {
  console.log(cnts);
})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
