// deletes all links from database

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});
const del = _.wrapCallback(db.del.bind(db));

_(db.createKeyStream({
  gt: 'link\x00',
  lt: 'link\xff',
}))
.flatMap(key => del(key))
.each(() => {

})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
