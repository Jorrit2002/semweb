
import file_operations as fo
import sys
import xml.etree.ElementTree as ET
from word import *
from geonames import *
import json
import plyvel
import nltk
from nltk.collocations import *
from nltk.metrics.spearman import *
from nltk.metrics import *
# from sklearn.metrics import cohen_kappa_score
# from sklearn.metrics import cohen_kappa_score
# from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
import collections


db = plyvel.DB('../db', create_if_missing=True)
# evaluation_db = plyvel.DB('../evaluation_db', create_if_missing=True)
# evaluation_db = plyvel.DB('../evaluation_2_db', create_if_missing=True)


def read_file(file_path, printing=False):
    """reads file and returns all words for file"""
    words = []

    tree = ET.parse(file_path)
    root = tree.getroot()

    if printing is True:
        print("\n")
        print("----------------")
        print(file_path)
        print("----------------")
        print("\n")
        print("{:<25} {:<15} {:<50} {:<15} {:<15} {:<15}".format(
            "id", "word", "sentence", "class", "entity", "confidence"))

    for text in root.findall('.//{http://ilk.uvt.nl/folia}text'):
        for p in text.findall('.//{http://ilk.uvt.nl/folia}p'):
            paragraph = p.find('.//{http://ilk.uvt.nl/folia}t').text
            paragraphId = p.attrib["{http://www.w3.org/XML/1998/namespace}id"]

            for s in p.findall('.//{http://ilk.uvt.nl/folia}s'):
                sentence = s.find('.//{http://ilk.uvt.nl/folia}t').text
                sentenceId = s.attrib["{http://www.w3.org/XML/1998/namespace}id"]

                for w in s.findall('.//{http://ilk.uvt.nl/folia}w'):
                    _id = w.attrib["{http://www.w3.org/XML/1998/namespace}id"]
                    _class = w.attrib["class"]
                    word = w.find('{http://ilk.uvt.nl/folia}t').text

                    w = Word(_id, word, paragraph, paragraphId, sentence, sentenceId, _class)

                    for entity in s.findall('.//{http://ilk.uvt.nl/folia}entity'):
                        entity_id = entity.attrib["{http://www.w3.org/XML/1998/namespace}id"]
                        entity_class = entity.attrib["class"]
                        entity_confidence = entity.attrib["confidence"]
                        obj = entity.find('.//{http://ilk.uvt.nl/folia}wref[@id=\''+w._id+'\']')
                        if obj is not None:
                            w.entity(entity_id, entity_class, entity_confidence)

                    words.append(w)
                    if printing is True:
                        print("{:<25} {:<15} {:<55} {:<15} {:<15} {:<15}".format(w._id[:20], w.word, w.sentence[:50], w._class, w.entity_class, w.entity_confidence))

    return words

def find_url_for_word(words):
    gn = GeoNames()
    for w in words:
        if w.entity_class == "loc":
            data = gn.findPlace(w.word)
            try:
                first_location = data["geonames"][0]["toponymName"]
                data = gn.findWiki(first_location)
                first_wiki = data["geonames"][0]["wikipediaUrl"]
                if first_wiki:
                    w.url = first_wiki
                    print(w.url)
            except:
                print("fail")

def find_geolocations(name="annotated"):
    gn = GeoNames()
    annotated_db = plyvel.DB('../{}_db'.format(name), create_if_missing=False)

    for key, value in annotated_db:

        if value.decode('utf-8') is 'None':
            continue

        byte_word = db.get(key)
        if byte_word is not None:

            sentence_key = '\x00'.join(key.decode('utf-8').split('\x00')[:-2])
            sentence_start = str.encode(sentence_key + '\x00')
            sentence_end = str.encode(sentence_key + '\xff')

            sentence = []

            for sentence_key, sentence_value in db.iterator(start=sentence_start,
                                                            include_start=True,
                                                            stop=sentence_end,
                                                            include_stop=True):
                sentence_data = json.loads(sentence_value.decode('utf-8'))
                sentence.append(sentence_data["text"])

        else:
            continue

        object = json.loads(byte_word.decode('utf-8'))

        if "normalisedText" not in object.keys():
            continue

        word = object['normalisedText']

        candidates = []

        data = gn.findPlace(word)
        print(data)
        if "geonames" in data.keys():
            for name in data["geonames"]:
                # print(name["toponymName"])
                searchName = name["toponymName"]
                # try:
                wiki_data = gn.findWiki(searchName)
                if "geonames" in wiki_data.keys():
                    for wiki in wiki_data["geonames"]:
                        # print(wiki)
                        candidates.append({ 'searchName': searchName, 'summary': wiki["summary"], 'url': wiki["wikipediaUrl"] })
            # except:
            #     print("no success")

        # print("{:<40} {:<40}".format(word, candidates[0]))
        if len(candidates) > 0:
            evaluation_db.put(key, str.encode(candidates[0]["url"]))
        else:
            evaluation_db.put(key, str.encode('None'))

def corpus_stats(corpus):
    """prints stats about xml files"""

    total = 0
    print("\n")
    print("{:<20} words {:>5}".format("words", "file"))
    for data in corpus:
        total += len(data["words"])
        print("{:<20} words {:>5}".format(len(data["words"]), data["path"]))
    print("{} number of words".format(total))


def filter_entities(corpus, entity="loc"):
    counter = 0
    for data in corpus:
        for w in data["words"]:
            if w.entity_class == entity:
                print("{:<15}{:<15}{:<15}".format(w.word, w.entity_class, w.entity_confidence))
                print(w.sentence)
                print("\n")
                counter += 1
    print("-------")
    print("{} words with the entity {}".format(counter, entity))


def print_sentences(corpus):
    for data in corpus:
        text = ""
        prev_sentence = ""

        for w in data["words"]:
            if prev_sentence is not w.sentence:
                prev_sentence = w.sentence
                text += w.sentence
        print(data["path"])
        print(text)


def write_to_db(corpus):
    for data in corpus:
        file_name = data["path"].split('/')[-1]

        # textName:paragraafId tekst
        # textName:paragraafId:zinsId tekst
        # textName:paragraafId:zinsId:wordId { tekst, normalisedTekst, kandidaten: [[dbpediaLink, geonameLink]] }

        for w in data["words"]:
            # db = leveldb.LevelDB('./db')
            # db.put(str.encode('{}:{}'.format(file_name, w.paragraphId)), str.encode(w.paragraph))
            # db.put(str.encode('{}:{}:{}'.format(file_name, w.paragraphId, w.sentenceId)), str.encode(w.sentence))
            db.put(str.encode("txt\x00{}".format(w._id.replace('.', '\x00'))),
                str.encode(json.dumps({
                    'text': w.word,
                    'normalisedText': w.word,
                    'nlWordClass': w._class,
                    'nlEntityClass': w.entity_class,
                    'nlEntityId': w.entity_id,
                    'nlEntityConfidence': w.entity_confidence,
                })))


def bigram_collocations(tokens, number=20, type="raw"):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(tokens)
    if type == "pmi":
        list = finder.nbest(bigram_measures.pmi, number)
    elif type == "chi_sq":
        list = finder.nbest(bigram_measures.chi_sq, number)
    else:
        list = finder.nbest(bigram_measures.raw_freq, number)
    return list


def find_bigram_collocations():
    """searches bigram collocations"""

    tokens = []
    for key, value in db:
        data = json.loads(value.decode("utf-8"))
        if data["nl_word_class"] not in ["PUNCTUATION", "NUMBER"]:
            tokens.append(data["word"])
    # print(tokens)
    print(bigram_collocations(tokens, number=50, type="pmi"))
    print(bigram_collocations(tokens, number=50, type="chi_sq"))


def find_entity_collocations():
    """combines words with same entities"""

    prev_key = None
    prev_data = None
    prev_prev_key = None
    prev_prev_data = None

    #landt -> land
    #HOOFDLETTERS -> Hoofdletter
    #DEN HAAG ->

    for key, value in db.iterator(start=b'txt\x00',
            include_start=True,
            stop=b'txt\xff',
            include_stop=False):

        data = json.loads(value.decode("utf-8"))

        # if data is not None and prev_data is not None:
            # print("new data set")
            # print("data, prevdata")
            # print("{} {}".format(data["text"], prev_data["text"]))


        if prev_data is not None and prev_prev_data is not None:
            # beetje extra checken, maar heb geen zin in apart logica voor begin en eind

            # if alle drie
            if (prev_data["nlEntityId"] == data["nlEntityId"]
                and data["nlEntityId"] == prev_prev_data["nlEntityId"]
                and prev_prev_data["nlEntityId"] != ''
                and prev_data["nlEntityId"] != ''
                and data["nlEntityId"] != ''
                and prev_prev_data["text"][0].isupper()
                and prev_data["text"][0].isupper()
                and data["text"][0].isupper()) :
                # print("alle drie {} {} {}".format(prev_prev_data["text"], prev_data["text"], data["text"]))
                combinedText = "{} {} {}".format(prev_prev_data["text"], prev_data["text"], data["text"])
                data["normalisedText"] = combinedText
                prev_data["normalisedText"] = combinedText
                prev_prev_data["normalisedText"] = combinedText
                combined_data = str.encode(json.dumps(data))
                combined_prev_data = str.encode(json.dumps(prev_data))
                combined_prev_prev_data = str.encode(json.dumps(prev_prev_data))
                db.put(prev_prev_key, combined_prev_prev_data)
                db.put(prev_key, combined_prev_data)
                db.put(key, combined_data)

            # eerste twee
            elif (prev_data["nlEntityId"] == prev_prev_data["nlEntityId"]
                and prev_prev_data["nlEntityId"] != ''
                and prev_data["nlEntityId"] != ''
                and prev_prev_data["text"][0].isupper()
                and prev_data["text"][0].isupper()) :
                # print("eerste twee {} {}".format(prev_prev_data["text"], prev_data["text"]))
                combinedText = "{} {}".format(prev_prev_data["text"], prev_data["text"])
                prev_data["normalisedText"] = combinedText
                prev_prev_data["normalisedText"] = combinedText
                combined_prev_data = str.encode(json.dumps(prev_data))
                combined_prev_prev_data = str.encode(json.dumps(prev_prev_data))
                db.put(prev_prev_key, combined_prev_prev_data)
                db.put(prev_key, combined_prev_data)

            # laatste twee
            elif (prev_data["nlEntityId"] == data["nlEntityId"]
                and prev_data["nlEntityId"] != ''
                and data["nlEntityId"] != ''
                and prev_data["text"][0].isupper()
                and data["text"][0].isupper()) :
                # print("laatste twee {} {}".format(prev_data["text"], data["text"]))
                combinedText = "{} {}".format(prev_data["text"], data["text"])
                data["normalisedText"] = combinedText
                prev_data["normalisedText"] = combinedText
                combined_data = str.encode(json.dumps(data))
                combined_prev_data = str.encode(json.dumps(prev_data))
                db.put(prev_key, combined_prev_data)
                db.put(key, combined_data)

            else:
                data["normalisedText"] = data["text"]
                combined_data = str.encode(json.dumps(data))
                db.put(key, combined_data)
        # if data is not None and prev_data is not None:
        #     print("{} {}".format(data["text"], prev_data["text"]))
        prev_prev_data = prev_data
        prev_prev_key = prev_key
        prev_key = key
        prev_data = data
        # if data is not None and prev_data is not None:
        #     print("{} {}".format(data["text"], prev_data["text"]))
        # print("----")

def normalise():

    find_entity_collocations()
    #
    for key, value  in db.iterator(start=b'txt\x00',
            include_start=True,
            stop=b'txt\xff',
            include_stop=True):
        data = json.loads(value.decode('utf-8'))
        normalised_text = data["normalisedText"]

        # if normalised_text.isupper():
        splitted = normalised_text.split()
        splitted_case = []
        for word in splitted:
            if word.isupper():
                new_word = ""
                for i in range(len(word)):
                    if i == 0:
                        new_word += word[i].upper()
                    else:
                        new_word += word[i].lower()
                splitted_case.append(new_word)
            else:
                splitted_case.append(word)
        normalised_text = ' '.join(splitted_case)

        # added in altnames
        # normalised_text = normalised_text.replace('landt', 'land')
        # normalised_text = normalised_text.replace('Landt', 'Land')

        # print(normalised_text)

        data["normalisedText"] = normalised_text
        encoded_data = str.encode(json.dumps(data))
        db.put(key, encoded_data)


def get(argv):
    print('get!')
    content = ""
    db = plyvel.DB('../annotated_db', create_if_missing=False)

    for key, value in db.iterator(
        start=b'txt\x00',
        include_start=True,
        stop=b'txt\xff',
        include_stop=True):

        print(key, value)
        # data = json.loads(value.decode("utf-8"))
        # print(data["normalisedText"])
    #     content += data["word"]+" "
    # print(content)


def evaluate(name="annotated"):
    """evaluates system output against annotated data"""

    annotated_db = plyvel.DB('../{}_db'.format(name), create_if_missing=False)

    reference = []
    test = []

    for key, value in annotated_db:
        #@TODO add correct key
        evaluate_key = str.encode('\x00'.join(key.decode('utf-8').split('\x00')))
        system_output = evaluation_db.get(evaluate_key)


        if system_output is None:
            system_output = str.encode('None')

        #normalise links
        value = value.decode('utf-8').replace('http://', 'https://')
        system_output = system_output.decode('utf-8').replace('http://', 'https://')

        print(value, system_output)

        reference.append(value)
        test.append(system_output)

    reference_set = reference_set = set(reference)
    test_set = set(test)

    print("reference count {}".format(len(reference)))
    print("test count {}".format(len(test)))
    print("reference_set count {}".format(len(reference_set)))
    print("test_set count {}".format(len(test_set)))

    print("Calculating metrics - {} values".format(len(reference)))
    # print("-----------------------------------")
    # print("Accuracy: {}".format(accuracy(reference, test)))
    #
    # print("-----------------------------------")
    # print("Precision: {}".format(precision(reference_set, test_set)))
    # print("Recall: {}".format(recall(reference_set, test_set)))
    # print("F1-score: {}".format(f_measure(reference_set, test_set)))
    # print("-----------------------------------")

    print("--SKLEARN--------------------------")
    print("Accuracy: {}".format(accuracy_score(reference, test)))
    print("Precision: {}".format(precision_score(reference, test, average='weighted')))
    print("Recall: {}".format(recall_score(reference, test, average='weighted')))
    print("F1-score: {}".format(f1_score(reference, test, average='weighted')))
    print("-----------------------------------")

def evaluate_and_report(name="evaluation_db"):
    """Evaluates system output against annotated data. Only words that are annotated as location are evaluated"""

    annotated_db = plyvel.DB('../annotated_test_db', create_if_missing=False)
    test_db = plyvel.DB('../{}'.format(name), create_if_missing=False)

    reference = []
    test = []

    found_in_candidates = []

    for key, value in annotated_db:

        evaluate_key = str.encode('\x00'.join(key.decode('utf-8').split('\x00')))
        system_output = test_db.get(evaluate_key)

        data = db.get(key)
        if data is None:
            continue
        word = json.loads(data.decode('utf-8'))["normalisedText"]

        if system_output is not None:
            system_data = json.loads(system_output.decode("utf-8"))
        else:
            system_data = { "link": "None", "candidates" : [] }


        if value.decode('utf-8') == "None":
            reference.append(False)
            if system_data["link"] == "None" or system_output is None:
                test.append(False)
            else:
                #print false positive
                print(word, value.decode('utf-8'), system_data["link"])
                test.append(True)
        else:
            reference.append(True)
            # print(value.decode('utf-8'), system_data["link"])

            if system_data["link"] == value.decode('utf-8'):
                test.append(True)
            else:
                in_candidates = False
                for candidate in system_data["candidates"]:
                    if value.decode('utf-8') == candidate["link"]:
                        found_in_candidates.append({ "normalised": word, "key": key, "gold": value.decode('utf-8'), "link":  system_data["link"], "candidates": system_data["candidates"] })
                        in_candidates = True
                        break
                test.append(False)
                #print false negative
                # print("False negative")
                print(key, "{:<30} {:<50} {:<60} {:<15}".format( word, value.decode('utf-8'), system_data["link"], in_candidates))


    print('\n')
    report(reference, test)


    print("------------")
    for found in found_in_candidates:
        print('{:<20} {:<20} {:<20}'.format(word, found["gold"], found["link"]))
        for candidate in found["candidates"]:
            print("-> {} {} {}".format(candidate["featureCode"], candidate["featureClass"], candidate["link"]))
    print("------------")
    print("{} found in candidates".format(len(found_in_candidates)))

def evaluate_features(name="evaluation_db"):
    """Evaluates system output against annotated data. Only words that are annotated as location are evaluated"""

    annotated_db = plyvel.DB('../annotated_db', create_if_missing=False)
    test_db = plyvel.DB('../{}'.format(name), create_if_missing=False)

    classes = []
    codes = []


    for key, value in annotated_db:

        evaluate_key = str.encode('\x00'.join(key.decode('utf-8').split('\x00')))
        system_output = test_db.get(evaluate_key)



        data = db.get(key)
        if data is None:
            continue
        word = json.loads(data.decode('utf-8'))["normalisedText"]

        if system_output is not None:
            system_data = json.loads(system_output.decode("utf-8"))
        else:
            system_data = { "link": "None", "candidates" : [] }

        for candidate in system_data["candidates"]:
            if value.decode('utf-8') == candidate["link"]:
                classes.append(candidate["featureClass"])
                codes.append(candidate["featureCode"])

    print(" --- Feature classes --- ")
    print(collections.Counter(classes))
    print(" --- Feature codes --- ")
    print(collections.Counter(codes))


def evaluate_only_links(name="annotated"):
    """Evaluates system output against annotated data. Only words that are annotated as location are evaluated"""

    annotated_db = plyvel.DB('../{}_db'.format(name), create_if_missing=False)

    reference = []
    test = []

    for key, value in annotated_db:
        #@TODO add correct key
        if value.decode('utf-8') == "None":
            continue

        data = db.get(key)
        #word = json.loads(data.decode('utf-8'))["normalisedText"]
        evaluate_key = str.encode('\x00'.join(key.decode('utf-8').split('\x00')))
        system_output = evaluation_db.get(evaluate_key)

        if system_output == "None":
            system_output = str.encode(system_output)

        if system_output is None:
            system_output = str.encode("None")


        #normalise links
        value = value.decode('utf-8').replace('http://', 'https://')
        system_output = system_output.decode('utf-8').replace('http://', 'https://')

        print(value, system_output)

        reference.append(value)
        test.append(system_output)

    reference_set = reference_set = set(reference)
    test_set = set(test)

    print("reference count {}".format(len(reference)))
    print("test count {}".format(len(test)))
    print("reference_set count {}".format(len(reference_set)))
    print("test_set count {}".format(len(test_set)))


    print("Calculating metrics - {} values".format(len(reference)))
    print("-----------------------------------")

    # print("-----------------------------------")
    # print("Accuracy: {}".format(accuracy(reference, test)))
    # print("Precision: {}".format(precision(reference_set, test_set)))
    # print("Recall: {}".format(recall(reference_set, test_set)))
    # print("F1-score: {}".format(f_measure(reference_set, test_set)))
    # print("-----------------------------------")

    print("--SKLEARN--------------------------")
    print("Accuracy: {}".format(accuracy_score(reference, test)))
    print("Precision: {}".format(precision_score(reference, test, average='weighted')))
    print("Recall: {}".format(recall_score(reference, test, average='weighted')))
    print("F1-score: {}".format(f1_score(reference, test, average='weighted')))
    print("-----------------------------------")


def evaluate_capitals(corpus):
    """compares nederlab location annotation with own annotation"""
    db = plyvel.DB('../annotated_db', create_if_missing=False)
    reference = []
    test = []

    for data in corpus:

        for w in data["words"]:

            annotation = db.get(str.encode("{}{}".format('txt\x00', w._id.replace('.', '\x00'))))
            if annotation is None:
                continue

            if annotation.decode('utf-8') != "None":
                reference.append(True)
            else:
                reference.append(False)

            print(w.word, w.word[0].isupper())
            if w.word[0].isupper():
                test.append(True)
            else:
                print("0----0i0s9u90isudhfajkdflkjshalkjdfhlkjsdhafljkhsldkjh")
                test.append(False)


            print(annotation.decode('utf-8'), w.entity_class)


    report(reference, test)

    reference_set = set(reference)
    test_set = set(test)

    print("reference count {}".format(len(reference)))
    print("test count {}".format(len(test)))
    print("reference_set count {}".format(len(reference_set)))
    print("test_set count {}".format(len(test_set)))

    print("Calculating metrics Nederlab - {} values".format(len(reference)))
    # print("-----------------------------------")
    # print("-----------------------------------")
    # print("Accuracy: {}".format(accuracy(reference, test)))
    # print("Precision: {}".format(precision(reference_set, test_set)))
    # print("Recall: {}".format(recall(reference_set, test_set)))
    # print("F1-score: {}".format(f_measure(reference_set, test_set)))
    # print("-----------------------------------")

    print("--SKLEARN--------------------------")
    print("Accuracy: {}".format(accuracy_score(reference, test)))
    print("Precision: {}".format(precision_score(reference, test, average='weighted')))
    print("Recall: {}".format(recall_score(reference, test, average='weighted')))
    print("F1-score: {}".format(f1_score(reference, test, average='weighted')))
    print("-----------------------------------")


def report(reference, test):

    tp = 0
    tn = 0
    fp = 0
    fn = 0

    print("---- own calculations -----")

    for i in range(len(test)):
        if test[i] == reference[i]:
            if reference[i] is True:
                tp += 1
            else:
                tn += 1
        else:
            if test[i] is True:
                fp += 1
            else:
                fn += 1

    print(tp, tn, fp, fn)

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)

    print("----------")
    print("precicion", precision)
    print("recall:", recall)
    print("f1score:", 2 * (precision * recall) / (precision + recall))
    print("----------")


def evaluate_nederlab(corpus):
    """compares nederlab location annotation with own annotation"""
    db = plyvel.DB('../annotated_db', create_if_missing=False)
    reference = []
    test = []

    locs = 0

    for data in corpus:

        for w in data["words"]:

            # only words annotated as location by nederlab
            # if w.entity_class != "loc":
            #     continue

            annotation = db.get(str.encode("{}{}".format('txt\x00', w._id.replace('.', '\x00'))))
            if annotation is None:
                continue
            else:
                if annotation.decode('utf-8') != "None":
                    reference.append(True)
                else:
                    reference.append(False)

            if w.entity_class == "loc":
                locs += 1
                test.append(True)
            else:
                test.append(False)

            print(annotation.decode('utf-8'), w.entity_class)



    print('locations tagged by nederlab: {}'.format(locs))

    report(reference, test)
    # for i in range(len(test)):
    #     if test[i] == reference[i]:
    #         if reference[i] is True:
    #             tp += 1
    #         else:
    #             tn += 1
    #     else:
    #         if test[i] is True:
    #             fp += 1
    #         else:
    #             fn += 1
    #
    # print(tp, tn, fp, fn)
    #
    # precision = tp / (tp + fp)
    # recall = tp / (tp + fn)
    #
    # print("----------")
    # print("----------")
    # print("precicion", precision)
    # print("recall:", recall)
    # print("f1score:",  2 * (precision * recall) / (precision + recall))

    print("----------")

    reference_set = set(reference)
    test_set = set(test)

    print("reference count {}".format(len(reference)))
    print("test count {}".format(len(test)))
    print("reference_set count {}".format(len(reference_set)))
    print("test_set count {}".format(len(test_set)))

    print("Calculating metrics Nederlab - {} values".format(len(reference)))
    print("-----------------------------------")
    # print("-----------------------------------")
    # print("Accuracy: {}".format(accuracy(reference, test)))
    # print("Precision: {}".format(precision(reference_set, test_set)))
    # print("Recall: {}".format(recall(reference_set, test_set)))
    # print("F1-score: {}".format(f_measure(reference_set, test_set)))
    # print("-----------------------------------")

    # print(test[:30])
    # print(reference[:30])

    print("--SKLEARN--------------------------")
    print("Accuracy: {}".format(accuracy_score(reference, test)))
    print("Precision: {}".format(precision_score(reference, test, average='weighted')))
    print("Recall: {}".format(recall_score(reference, test, average='weighted')))
    print("F1-score: {}".format(f1_score(reference, test, average='weighted')))
    print("-----------------------------------")

def cohens_kappa(argv):
    """calculates cohens kappa between two given annotaters"""

    db1 = plyvel.DB('../{}_db'.format(argv[2]), create_if_missing=False)
    db2 = plyvel.DB('../{}_db'.format(argv[3]), create_if_missing=False)

    values1 = []
    values2 = []

    for key, value in db1:
        value2 = db2.get(key)
        values1.append(value)
        values2.append(value2)

    # agreement.Ae_kappa(values1, values2)
    kappa = cohen_kappa_score(values1, values2)
    print("Inter annotator agreement: {}".format(kappa))


def merge_annotations():
    """merge annotations. Inagreements are evaluated manually in console"""

    merged_db = plyvel.DB('../annotated_test_db', create_if_missing=True)
    db1 = plyvel.DB('../{}_db'.format("jorrit_test"), create_if_missing=False)
    db2 = plyvel.DB('../{}_db'.format("jorrit"), create_if_missing=False)

    found = not_found = annotated_test_count = annotated_count = 0

    for key, value in db2:
        annotated_count += 1

    for key, value in db1:
        annotated_test_count += 1

    for key, value in db1:

        value2 = db2.get(key)

        if value2 is None:
            not_found += 1
        else:
            found += 1
        continue
        # if value2 is None:
        #     merged_db.put(key, value)

        if value == value2:
            merged_db.put(key, value)
        else:
            sentence_key = '\x00'.join(key.decode('utf-8').split('\x00')[:-2])
            sentence_start = str.encode(sentence_key + '\x00')
            sentence_end = str.encode(sentence_key + '\xff')
            sentence = []

            for sentence_key, sentence_value in db.iterator(start=sentence_start,
                                                            include_start=True,
                                                            stop=sentence_end,
                                                            include_stop=True):
                sentence_data = json.loads(sentence_value.decode('utf-8'))
                sentence.append(sentence_data["text"])

            data = db.get(key)
            if data is None:
                continue
            else:
                word = json.loads(data.decode('utf-8'))["normalisedText"]

            print("\n\n----------")
            print(' '.join(sentence))
            print("----------")
            print("word: " + word)
            print("----------")
            print("J) {} \nR) {}\n".format(value.decode('utf-8'), value2.decode('utf-8')))
            i = input("J/R/URL>")
            if i == "J":
                merged_db.put(key, value)
            elif i == "R":
                merged_db.put(key, value2)
            else:
                merged_db.put(key, str.encode(i))
    print(found, not_found, annotated_count, annotated_test_count)

def read_corpus():
    """reads xml files into a dictionary"""

    corpus = []
    file_paths = fo.get_file_paths("src/folia")
    total = len(file_paths)

    for path in file_paths:
        words = read_file(path)
        # find_url_for_word(words)
        corpus.append({"path": path, "words": words})

    return corpus

def main(argv):

    if "--get" in argv:
        get(argv)
        return

    corpus = []


    if "--stats" in argv:
        if corpus == []:
            corpus = read_corpus()
        corpus_stats(corpus)
    if "--insert-db" in argv:
        if corpus == []:
            corpus = read_corpus()
        write_to_db(corpus)
    if "--collocations" in argv:
        find_bigram_collocations()
    if "--entity-collocations" in argv:
        find_entity_collocations()
    if "--filter" in argv:
        if corpus == []:
            corpus = read_corpus()
        filter_entities(corpus)
    if "--evaluate" in argv:
        evaluate()
    if "--evaluate-links" in argv:
        evaluate_only_links()
    if "--evaluate-report" in argv:
        evaluate_and_report(argv[2])
    if "--evaluate-nederlab" in argv:
        if corpus == []:
            corpus = read_corpus()
        evaluate_nederlab(corpus)
    if "--evaluate-capitals" in argv:
        if corpus == []:
            corpus = read_corpus()
        evaluate_capitals(corpus)
    if "--kappa" in argv:
        cohens_kappa(argv)
    if "--merge" in argv:
        merge_annotations()
    if "--normalise" in argv:
        normalise()
    if "--geolocations" in argv:
        find_geolocations(argv[2])
    if "--features" in argv:
        evaluate_features(argv[2])

    db.close()

if __name__ == '__main__':
    main(sys.argv)
