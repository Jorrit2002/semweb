// first round of annotation used wikipedia, this converts to dbpedia.

const _ = require('highland');
const level = require('level');

const db = level('./remko_test_db', {
  valueEncoding: 'utf8',
});
const put = _.wrapCallback(db.put.bind(db));

_(db.createReadStream())
.filter(({ value }) => value !== 'None' && value !== 'not found')
.map(({ key, value }) => {
  const match = /^https?:\/\/[a-z][a-z].wikipedia.org\/wiki\/(.*)/.exec(value);
  if (match === null) {
    console.log(`no match on ${value}`);
    return ({ key, value });
  } else {
    return ({ key, value: `http://dbpedia.org/page/${encodeURIComponent(match[1])}` });
  }
})
.tap(_.log)
.flatMap(({ key, value }) => put(key, value))
.each(() => {})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
