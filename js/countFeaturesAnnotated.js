// counts the feature class and feature codes of the GeoName entities in the annotated development
// dataset by using the mapping in the db from url back to GeoName entity; the mapping is functional

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});
const annoDb = level('./annotated_db', {
  keyEncoding: 'utf8',
  valueEncoding: 'utf8',
});

const get = _.wrapCallback(db.get.bind(db));

_(annoDb.createValueStream({
}))
.filter(value => value !== 'None' && value !== 'not found')
.flatMap(value => get(`url\x00${value}`))
.errors((err) => { console.log(err); })
.flatMap(geoId => get(`geo\x00${geoId}`))
.errors((err) => { console.log(err); })
.reduce({ class: {}, code: {}, cnt: 0 }, (acc, { featureClass, featureCode }) => {
  const classCount = acc.class[featureClass];
  acc.class[featureClass] = classCount ? classCount + 1 : 1;
  const codeCount = acc.code[featureCode];
  acc.code[featureCode] = codeCount ? codeCount + 1 : 1;
  acc.cnt += 1;
  return acc;
})
.each((counts) => { console.log(counts); })
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
