// inspect alternate name matches with words in the text ordered by occurence count
// used to remove errors that occur often

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});

// for all the words
_(db.createReadStream({
  gt: 'txt\x00',
  lt: 'txt\xff',
}))
.flatMap(({ key, value: { normalisedText } }) =>
    _(db.createValueStream({
      gte: `alt\x00${normalisedText}\x00`,
      lte: `alt\x00${normalisedText}\x00\xff`,
    }))
  .reduce(new Set(), (acc, val) => {
    acc.add(val);
    return acc;
  })
  .filter(set => set.size > 0)
  // copy ids into object
  .map(candidates => ({ key, value: { normalisedText, candidates } }))
)
.group(({ value: { normalisedText } }) => normalisedText)
.flatMap(groups =>
  _(Object.keys(groups))
  .map((key) => {
    const freq = groups[key].length;
    return [key, freq];
  })
)
.sortBy((a, b) => b[1] - a[1])
.take(500)
.tap(([key, freq]) => process.stdout.write(`'${key} ${freq}', `))
.each(() => {})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
