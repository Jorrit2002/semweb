"""
All function related to file operations
"""
import os
import sys
from word import *

def get_file_paths(file_name):

    root = os.path.dirname(os.path.abspath(__file__))
    f = []

    for path, subdirs, files in os.walk(root):
        for name in files:
            if name == ".DS_Store":
                continue
            if file_name in path:
                f.append(path+"/"+name)

    return f

def read_file(file_path):
    content = []
    with open(file_path) as file:
        for line in file.readlines():
            content.append(line.split())

    return content

def create_file(file_path, file_content):
    with open(file_path, "w") as file:
        for w in file_content:
            file.write("{}\t{}\t{}\t{}\n".format(w._id, w.word, "", w.url))
