const _ = require('highland');
const level = require('level');
const argv = require('minimist')(process.argv.slice(2));

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});
const evalDb = level(`./evaluation_db_${argv.s}`, {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});
const putEval = _.wrapCallback(evalDb.put.bind(evalDb));
const get = _.wrapCallback(db.get.bind(db));


// wordid -> { link, candidates:
//  [{ link, name, asciiName, lat, long, featureClass, featureCode, cc, population }] }

/*
class:
{ A: 36, P: 20, T: 7, H: 6, S: 1},
code:
{ PCLI: 14,
  PCLD: 12,
  ADM1: 8,
  PPLA: 7,
  PPL: 6,
  ISL: 4,
  STRT: 4,
  PPLC: 4,
  CAPE: 3,
  PPLL: 2,
  BAY: 2,
  ADM2: 2,
  PPLA3: 1,
  FCL: 1
*/

function sortUrl(a, b) {
  // met link gaat voor zonder link
  if (a.link === 'not found' && b.link !== 'not found') {
    return 1;
  }
  if (a.link !== 'not found' && b.link === 'not found') {
    return -1;
  }
  return 0;
}
function sortClass(a, b) {
  // A > P > T > H > S
  if (a.link === 'not found' && b.link !== 'not found') {
    return 1;
  }
  if (a.link !== 'not found' && b.link === 'not found') {
    return -1;
  }
  if (a.featureClass === 'A') {
    if (b.featureClass === 'A') {
      return 0;
    }
    return -1;
  }
  if (a.featureClass === 'P') {
    if (b.featureClass === 'A') {
      return 1;
    }
    if (b.featureClass === 'P') {
      return 0;
    }
    return -1;
  }
  if (a.featureClass === 'T') {
    if (b.featureClass === 'A') {
      return 1;
    }
    if (b.featureClass === 'P') {
      return 1;
    }
    if (b.featureClass === 'T') {
      return 0;
    }
    return -1;
  }
  if (a.featureClass === 'T') {
    if (b.featureClass === 'A') {
      return 1;
    }
    if (b.featureClass === 'P') {
      return 1;
    }
    if (b.featureClass === 'T') {
      return 1;
    }
    if (b.featureClass === 'H') {
      return 0;
    }
    return -1;
  }
  if (a.featureClass === 'S') {
    if (b.featureClass === 'S') {
      return 0;
    }
    return 1;
  }
  return 0;
}

function sortCode(a, b) {
  if (a.featureCode === 'PCLI') {
    if (b.featureCode === 'PCLI') {
      return 0;
    }
    if (b.featureCode === 'PCLD') {
      return -1;
    }
    if (b.featureCode === 'ADM1') {
      return -1;
    }
    if (b.featureCode === 'PPLA') {
      return -1;
    }
    if (b.featureCode === 'PPL') {
      return -1;
    }
    if (b.featureCode === 'ISL') {
      return -1;
    }
    if (b.featureCode === 'STRT') {
      return -1;
    }
    if (b.featureCode === 'PPLC') {
      return -1;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'PCLD') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 0;
    }
    if (b.featureCode === 'ADM1') {
      return -1;
    }
    if (b.featureCode === 'PPLA') {
      return -1;
    }
    if (b.featureCode === 'PPL') {
      return -1;
    }
    if (b.featureCode === 'ISL') {
      return -1;
    }
    if (b.featureCode === 'STRT') {
      return -1;
    }
    if (b.featureCode === 'PPLC') {
      return -1;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'ADM1') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 0;
    }
    if (b.featureCode === 'PPLA') {
      return -1;
    }
    if (b.featureCode === 'PPL') {
      return -1;
    }
    if (b.featureCode === 'ISL') {
      return -1;
    }
    if (b.featureCode === 'STRT') {
      return -1;
    }
    if (b.featureCode === 'PPLC') {
      return -1;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'PPLA') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 0;
    }
    if (b.featureCode === 'PPL') {
      return -1;
    }
    if (b.featureCode === 'ISL') {
      return -1;
    }
    if (b.featureCode === 'STRT') {
      return -1;
    }
    if (b.featureCode === 'PPLC') {
      return -1;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'PPL') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 0;
    }
    if (b.featureCode === 'ISL') {
      return -1;
    }
    if (b.featureCode === 'STRT') {
      return -1;
    }
    if (b.featureCode === 'PPLC') {
      return -1;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'ISL') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 0;
    }
    if (b.featureCode === 'STRT') {
      return -1;
    }
    if (b.featureCode === 'PPLC') {
      return -1;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'STRT') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 0;
    }
    if (b.featureCode === 'PPLC') {
      return -1;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'PPLC') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 1;
    }
    if (b.featureCode === 'PPLC') {
      return 0;
    }
    if (b.featureCode === 'CAPE') {
      return -1;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'CAPE') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 1;
    }
    if (b.featureCode === 'PPLC') {
      return 1;
    }
    if (b.featureCode === 'CAPE') {
      return 0;
    }
    if (b.featureCode === 'PPLL') {
      return -1;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'PPLL') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 1;
    }
    if (b.featureCode === 'PPLC') {
      return 1;
    }
    if (b.featureCode === 'CAPE') {
      return 1;
    }
    if (b.featureCode === 'PPLL') {
      return 0;
    }
    if (b.featureCode === 'BAY') {
      return -1;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'BAY') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 1;
    }
    if (b.featureCode === 'PPLC') {
      return 1;
    }
    if (b.featureCode === 'CAPE') {
      return 1;
    }
    if (b.featureCode === 'PPLL') {
      return 1;
    }
    if (b.featureCode === 'BAY') {
      return 0;
    }
    if (b.featureCode === 'ADM2') {
      return -1;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'ADM2') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 1;
    }
    if (b.featureCode === 'PPLC') {
      return 1;
    }
    if (b.featureCode === 'CAPE') {
      return 1;
    }
    if (b.featureCode === 'PPLL') {
      return 1;
    }
    if (b.featureCode === 'BAY') {
      return 1;
    }
    if (b.featureCode === 'ADM2') {
      return 0;
    }
    if (b.featureCode === 'PPLA3') {
      return -1;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'PPLA3') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 1;
    }
    if (b.featureCode === 'PPLC') {
      return 1;
    }
    if (b.featureCode === 'CAPE') {
      return 1;
    }
    if (b.featureCode === 'PPLL') {
      return 1;
    }
    if (b.featureCode === 'BAY') {
      return 1;
    }
    if (b.featureCode === 'ADM2') {
      return 1;
    }
    if (b.featureCode === 'PPLA3') {
      return 0;
    }
    if (b.featureCode === 'FCL') {
      return -1;
    }
  }
  if (a.featureCode === 'FCL') {
    if (b.featureCode === 'PCLI') {
      return 1;
    }
    if (b.featureCode === 'PCLD') {
      return 1;
    }
    if (b.featureCode === 'ADM1') {
      return 1;
    }
    if (b.featureCode === 'PPLA') {
      return 1;
    }
    if (b.featureCode === 'PPL') {
      return 1;
    }
    if (b.featureCode === 'ISL') {
      return 1;
    }
    if (b.featureCode === 'STRT') {
      return 1;
    }
    if (b.featureCode === 'PPLC') {
      return 1;
    }
    if (b.featureCode === 'CAPE') {
      return 1;
    }
    if (b.featureCode === 'PPLL') {
      return 1;
    }
    if (b.featureCode === 'BAY') {
      return 1;
    }
    if (b.featureCode === 'ADM2') {
      return 1;
    }
    if (b.featureCode === 'PPLA3') {
      return 1;
    }
    if (b.featureCode === 'FCL') {
      return 0;
    }
  }
}

_(db.createReadStream({
  gt: 'txt\x00',
  lt: 'txt\xff',
}))
.flatMap(({ key, value: { candidates } }) => {
  // was bug in herschrijven evalution db's, filter did not output None
  // kon ook door eval db weg te gooien
  if (candidates.length === 0) {
    return putEval(key, {
      link: 'None',
      candidates: [],
    });
  }
  return _(candidates)
  .flatMap(candidate =>
    get(`link\x00${candidate}`)
    // link not found
    .errors((err, push) => {
      if (err.notFound) {
        push(null, 'not found');
      }
    })
    .flatMap(link =>
      get(`geo\x00${candidate}`)
      .map((geoObj) => {
        geoObj.link = link;
        return geoObj;
      })
    )
  )
  .reduce([], (acc, val) => acc.concat(val))
  .map((geoObjs) => {
    let sortedCandidates = [];
    if (argv.s === 'naive') {
      sortedCandidates = geoObjs;
    }
    if (argv.s === 'url') {
      sortedCandidates = geoObjs.sort(sortUrl);
    }
    if (argv.s === 'class') {
      sortedCandidates = geoObjs.sort(sortUrl).sort(sortClass);
    }
    if (argv.s === 'code') {
      sortedCandidates = geoObjs.sort(sortUrl).sort(sortCode);
    }
    if (argv.s === 'all') {
      sortedCandidates = geoObjs.sort(sortUrl).sort(sortClass).sort(sortCode);
    }
    return {
      link: sortedCandidates[0].link,
      candidates: sortedCandidates,
    };
  })
  .tap(_.log)
  .flatMap(item => putEval(key, item));
})
.each(() => {})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
