// print counts for words that start with a capital
// sorted by count and showing Nederlab entity classification

const _ = require('highland');
const level = require('level');

const db = level('./db', {
  keyEncoding: 'utf8',
  valueEncoding: 'json',
});


// { key, value: {
//   text,
//   normalisedText,
//   nlWordClass,
//   nlEntityClass,
//   nlEntityConfidence,
//   nlEntityId
// }

// for all the words
_(db.createReadStream({
  gt: 'txt\x00',
  lt: 'txt\xff',
}))
// find the alternates
.filter(({ value: { normalisedText } }) => /^[A-Z].*/.test(normalisedText))
.group(({ value: { normalisedText } }) => normalisedText)
.flatMap(groups =>
  _(Object.keys(groups))
  .map((key) => {
    const freq = groups[key].length;
    const entClass = groups[key].reduce((acc, { value }) => acc.concat(value.nlEntityClass), []);
    return [key, freq, entClass];
  })
)
.sortBy((a, b) => b[1] - a[1])
.tap(([key, freq, entClass]) => console.log(` ${freq}\t${key}\t${entClass}`))
.reduce(0, (acc, [, freq]) => acc + freq)
.each((tot) => { console.log(`total ${tot}`); })
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
