import sys
from flask import Flask,redirect
from flask import render_template
from flask import request

from collections import defaultdict

import json
import plyvel


annotator = "remko_test"

app = Flask(__name__)
db = plyvel.DB('../../db', create_if_missing=True)
annotated_db = plyvel.DB('../../{}_db'.format(annotator), create_if_missing=True)

@app.route('/')
def start(name=None):

    data = get_next()

    return render_template('index.html', data=data, annotator=annotator)
    # return render_template('index.html', data=data)


@app.route('/annotate', methods=['POST'])
def annotate():
    if request.method == 'POST':

        print("-------annotate-------")
        print("request form: {}".format(request.form))
        annotated = request.form['annotate']
        key = request.form['wordId'].replace('.', '\x00')

        if "link" not in request.form and "manual_link" not in request.form and annotated == '1':
            print('redirect --> no link while annotating')
            return redirect("/", code=302)

        print("word key: {}".format(key))

        if annotated == '1':
            if "manual_link" in request.form:
                link = request.form["manual_link"]
                if link == "":
                    link = request.form['link']
            else:
                link = request.form['link']

            print("Save as geoname - url: {}".format(link))
            annotated_db.put(key.encode(), link.encode())
        else:
            print("not a geoname")
            annotated_db.put(key.encode(), b'None')

        print("-------end-annotate-------")

        return redirect("/", code=302)

    return render_template('success.html')



def get_next():
    counter = 0
    textCounter = {}
    for key, value in db.iterator(start=b'txt\x00', include_start=True, stop=b'txt\xff', include_stop=True):
        data = json.loads(value.decode('utf-8'))

        counter += 1

        # limit annotating words per text
        min_limit = 600
        limit = 750
        textKey = key.decode('utf-8').split('\x00')[1]

        # print(textKey)
        # print(textCounter)

        if textKey in textCounter.keys():
            textCounter[textKey] += 1
        else:
            textCounter[textKey] = 0

        # print("text: {}".format(textKey))
        # print("Word number in text: {}".format(textCounter[textKey]))
        # print(textCounter)

        if textCounter[textKey] >= limit or textCounter[textKey] < min_limit:
            continue


        # print("data text/:", data)

        word = data['normalisedText']

        #skip numbers and punctuation
        if "nlWordClass" in data.keys():
            if data["nlWordClass"] in ["PUNCTUATION", "NUMBER"]:
                continue

        #skip words starting with lower case
        if not word[0].isupper():
            continue

        if "nlEntityClass" in data.keys():
            nl_entity_class = data["nlEntityClass"]
        else:
            nl_entity_class = "Unknown"

        if "candidates" in data.keys():
            candidates = data['candidates']
        else:
            candidates = []
        links = []

        annotated_key = key

        if annotated_db.get(annotated_key) is None:
            sentence_key = '\x00'.join(key.decode('utf-8').split('\x00')[:-2])
            sentence_start = str.encode(sentence_key + '\x00')
            sentence_end = str.encode(sentence_key + '\xff')

            # print('\n--------------')
            # print('sentence key: ', sentence_key)
            # print('sentence start: ', sentence_start)
            # print('sentence end: ', sentence_end)
            # print('\n--------------')

            sentence = []

            for sentence_key, sentence_value in db.iterator(start=sentence_start,
                                        include_start=True,
                                        stop=sentence_end,
                                        include_stop=True):

                sentence_data = json.loads(sentence_value.decode('utf-8'))
                sentence.append(sentence_data["text"])

            for candidate in candidates:
                link = db.get(str.encode('link\x00{}'.format(candidate)))
                if link is not None:
                    link = link.decode('utf-8')

                    if link[0] == '"':
                        link = link[1:]
                    if link[-1] == '"':
                        link = link[:-1]
                    links.append(link)

            #Only annotate words with a candidate
            # if len(links) == 0:
            #     continue

            annotated_this = { 'wordId': annotated_key.decode('utf-8').replace('\x00', '.'),
					'sentence': ' '.join(sentence),
					'normalisedText': word,
					'candidates': candidates,
					'nl_entity_class': nl_entity_class,
					'links': links,
					'counter': counter
					}

            return annotated_this

def debug():
	for key, value in annotated_db.iterator(
			start=b'txt\x00',
			include_start=True,
			stop=b'txt\xff',
			include_stop=True):
		print(key.decode('utf-8'))
		print(value.decode('utf-8'))



def main(argv):

	if "--debug" in argv:
		debug()
	else:
	    app.run(port=5001)


if __name__ == '__main__':
    main(sys.argv)
