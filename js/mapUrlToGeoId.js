// writes mapping from url to GeoNames id form  GeoNames alterateNames data in leveldb

const fs = require('fs');
const _ = require('highland');
const level = require('level');

const db = level('./db', {
  valueEncoding: 'json',
});
const put = _.wrapCallback(db.put.bind(db));

// read file of alternateNames
_(fs.createReadStream('alternateNames.txt', { encoding: 'utf-8' }))
// split on new lines
.split()
// split into an array of values
// map line onto fields
// alternateNameId   : the id of this alternate name, int
// geonameid         : geonameId referring to id in table 'geoname', int
// isolanguage       : iso 639 language code 2- or 3-characters;
//                     4-characters 'post' for postal codes and 'iata','icao' and faac for airport
//                     codes, fr_1793 for French Revolution names,  abbr for abbreviation, link
//                     for a website, varchar(7)
// alternate name    : alternate name or name variant, varchar(400)
// isPreferredName   : '1', if this alternate name is an official/preferred name
// isShortName       : '1', if this is a short name like 'California' for 'State of California'
// isColloquial      : '1', if this alternate name is a colloquial or slang term
// isHistoric        : '1', if this alternate name is historic and was used in the past
.map(line => line.split('\t'))
// filter non-name alternates
.filter(([, , iso]) => iso === 'link')
.map(([, geoId, , link]) => {
  const match = /^https?:\/\/en.wikipedia.org\/wiki\/(.*)/.exec(link);
  const dbpedia = match === null ? match : `http://dbpedia.org/page/${match[1]}`;
  return [geoId, dbpedia];
})
.filter(([, link]) => link !== null)
.flatMap(([geoId, link]) => put(`url\x00${link}`, geoId))
.errors((err) => {
  console.log(err);
})
.each(() => {
  // process.stdout.write('.');
})
.done(() => {
  db.close();
  console.log('done');
  process.exit(0);
});
